#!/usr/bin/env bash

# trisquel script: installs build dependencies for PureOS 10, Trisquel 9 and Trisquel 10.
#
#	Copyright (C) 2014-2016, 2020-2021 Leah Rowe <info@minifree.org>
#	Copyright (C) 2016, Klemens Nanni <contact@autoboot.org>
#	Copyright (C) 2020, Wei Mingzhi <whistler_wmz@users.sf.net>
#	Copyright (C) 2021, madbehaviorus <mad.behaviorus@mailbox.org>
#	Copyright (C) 2021, Ron Nazarov <noisytoot@disroot.org>
#	Copyright (C) 2023, Adrien Bourmault <neox@gnu.org>
#	Copyright (C) 2023-2024 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, version 3 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

[ "x${DEBUG+set}" = 'xset' ] && set -v
set -u -e

to_install=""
install_packages()
{
    for package in $@ ; do
        if dpkg -l "${package}" | grep "^ii" 2>&1>/dev/null ; then
            continue
        else
            to_install="${to_install} ${package}"
        fi
    done
}

if [ $EUID -ne 0 ]; then
   printf "This script must be run as root\n" 
   exit 1
fi

# Duplications are intentional. Please do not re-factor.
#
# This is so that they can moved to separate scripts.
#

install_packages wget

# For downloading source code
# ------------------------------------------------------------

install_packages git

# For Tianocore and iPXE
# TODO: check whether this is the full list

install_packages uuid-dev nasm

# For building source code:
# ------------------------------------------------------------

install_packages build-essential

# for running the crostool script (to get mrc.bin file for t440p)
install_packages sharutils curl parted e2fsprogs unzip

# to use the right software versions and links for compiling 
install_packages libtool pkg-config

# for cross-compiling ARM binaries
install_packages gcc-arm-linux-gnueabi

[ "$(uname -i)" = x86_64 ] || [ "$(uname -m)" = x86_64 ]
arch=${?}

# For cross-compiling i686 target on x86_64 host.
if [ "${arch}" -eq 0 ];	then
	install_packages gcc-multilib libc6-i386 libc6-dev-i386
	install_packages lib32stdc++6 g++-multilib dh-autoreconf
fi

# Memtest86+ build dependencies
# ------------------------------------------------------------

install_packages build-essential python2.7 python-is-python3

# i945-pwm build dependencies
# ------------------------------------------------------------

install_packages build-essential perl

# Coreboot build dependencies (also requires build-essential and git)
# ------------------------------------------------------------

install_packages libncurses5-dev doxygen acpica-tools gdb flex bison build-essential git libssl-dev gnat

# GRUB build dependencies (also requires build-essential, bison and flex)
# ------------------------------------------------------------

# Trisquel 10 (nabia) doesn't have fonts-unifont. Also for the check
# we can assume that the 'nabia' code name is unique as it is in the
# upstream debootstrap in /usr/share/debootstrap/scripts/nabia.
if ! grep -q 'VERSION_CODENAME="nabia"' /etc/os-release ; then
    install_packages fonts-unifont
fi

install_packages libopts25 libselinux1-dev autogen m4 autoconf help2man libopts25-dev libfont-freetype-perl automake autotools-dev build-essential bison flex libfuse-dev liblzma-dev gawk libdevmapper-dev libtool libtool-bin libfreetype6-dev unifont

# BucTS build dependencies (external script)
# ------------------------------------------------------------

install_packages build-essential

# Flashrom build dependencies (also requires build-essential)
# ------------------------------------------------------------

install_packages libpci-dev pciutils zlib1g-dev libftdi-dev build-essential libusb-1.0-0 libusb-1.0-0-dev libusb-dev

# For cross-compiling i686 target on x86_64 host.
[ "${arch}" -eq 0 ] && install_packages lib32z1-dev

# For running make check
# ----------------------
install_packages shellcheck # lint
install_packages gawk git grep lzip make sed tar xz-utils # u-boot-libre

# For building the website tarball
install_packages \
    graphicsmagick \
    graphviz \
    pandoc \
    texinfo \
    texlive \
    texlive-plain-generic

if [ -n "${to_install}" ] ; then
    apt install -y ${to_install}
fi
