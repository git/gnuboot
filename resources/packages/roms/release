#!/usr/bin/env bash

#
#  helper script: generate release archive (ROM images)
#
#    Copyright (C) 2020,2021 Leah Rowe <info@minifree.org>
#    Copyright (C) 2023,2024 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
#    Copyright (C) 2024 Adrien Bourmault <neox@gnu.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# This script assumes that the working directory is the root
# of git or release archive

[ "x${DEBUG+set}" = 'xset' ] && set -v
set -u -e

projectname="$(cat projectname)"

version="version-unknown"
if [ -f version ]; then
    version="$(cat version)"
fi
versiondate="version-date-unknown"
if [ -f versiondate ]; then
    versiondate="$(cat versiondate)"
fi

if [ ! -d "bin/" ]; then
    ./build boot roms all
fi

[ ! -d "release/" ] && \
    mkdir -p release/
[ -d "release/roms/" ] && \
    rm -Rf "release/roms/"
[ ! -d "release/roms/" ] && \
    mkdir -p "release/roms/"
[ -d "release/roms-dbg/" ] && \
    rm -Rf "release/roms-dbg/"
[ ! -d "release/roms-dbg/" ] && \
    mkdir -p "release/roms-dbg/"

printf "Building ROM image archives for version %s\n" "${version}"

(
    # For consistency reasons, the same logic than in
    # resources/packages/roms/test is being used. If you improve the
    # code below, don't forget to also improve the test code.
    cd bin/
    for target in *; do
        if [ ! -d "${target}/" ]; then
            continue
        fi

        printf "Generating release/roms/%s-%s_%s.tar.xz\n" \
               "${projectname}" "${version}" "${target##*/}"
        printf "%s\n" "${version}" > "${target}/version"
        printf "%s\n" "${versiondate}" > "${target}/versiondate"
        printf "%s\n" "${projectname}" > "${target}/projectname"
        rm -f ../release/roms/"${projectname}"-"${version}"_"${target##*/}".tar

        tar -cf \
            ../release/roms/"${projectname}"-"${version}"_"${target##*/}".tar \
            "${target}"/*.rom \
            "${target}"/version \
            "${target}"/versiondate \
            "${target}"/projectname

        xz -vv -9e -f \
           ../release/roms/"${projectname}"-"${version}"_"${target##*/}".tar
    done
)

printf "\nROM image release archives available at release/roms/\n\n"

printf "Building ROM debug image archives for version %s\n" "${version}"

(
    # For consistency reasons, the same logic than above (look for a comment
    # with "For consistency reasons" above). Though note that at the time of
    # writing, there are no test yet for debug images.
    cd bin-dbg/
    for target in *; do
        if [ ! -d "${target}/" ]; then
            continue
        fi

        printf "Generating release/roms-dbg/%s-%s_debug_%s.tar.xz\n" \
               "${projectname}" "${version}" "${target##*/}"
        printf "%s\n" "${version}" > "${target}/version"
        printf "%s\n" "${versiondate}" > "${target}/versiondate"
        printf "%s\n" "${projectname}" > "${target}/projectname"
        rm -f ../release/roms-dbg/"${projectname}"-"${version}"_debug_"${target##*/}".tar

        tar -cf \
            ../release/roms-dbg/"${projectname}"-"${version}"_debug_"${target##*/}".tar \
            "${target}"/*.rom \
            "${target}"/version \
            "${target}"/versiondate \
            "${target}"/projectname \
            ../resources/coreboot/README.debug

        xz -vv -9e -f \
           ../release/roms-dbg/"${projectname}"-"${version}"_debug_"${target##*/}".tar
    done
)

printf "Building debug ROM image archives for version %s\n" "${version}"