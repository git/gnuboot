---
title: Documentation
...

Manuals
=======

The GNU Boot project started creating a manual
([online](../manual/gnuboot.html), [pdf](../manual/gnuboot.pdf)) to
better organize the documentation about GNU Boot. It also enables to
target a wider diversity of users, contributors and maintainers.

Note that the online manual can also be saved locally. In that case
it's a good idea to also save the
[SOIC-16.jpeg](../manual/images/SOIC-16.jpeg) picture in the same directory
to make the image(s) work.

General information
===================

-   Information about where to find GNU Boot news, including release
    announcements, can be found in the [news page](../news.md).
-   [Answers to Frequently Asked Questions about GNU Boot](../faq.md).

Installing and updating GNU Boot
================================

-   [What systems can I use GNU Boot on?](hardware/)
-   [How to install GNU Boot](install/)
-   [How do I know what version I'm running?](install/version.md)

Documentation related to operating systems
==========================================

-   [GNU+Linux Guides](gnulinux/)
-   [How to install BSD on a libreboot system](bsd/)

Information for developers
==========================

-   [How to compile the GNU Boot source code](build/)
-   [GNU Boot source code history and provenance](history/)
-   [Depthcharge payload](depthcharge/) (**Libreboot 20160907 only**)
-   [GRUB payload](grub/)

Other information
=================

-   [Miscellaneous](misc/)
-   [List of codenames](misc/codenames.md)
