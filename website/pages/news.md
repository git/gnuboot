---
title: News
...

The GNU Boot news contains projects announcements (both technical and
organizational) and also information about releases.

They are announced in several places:

- In the [gnuboot-announce mailing
  list](https://lists.gnu.org/mailman/listinfo/gnuboot-announce).

- In the (also available as an
  [atom](https://savannah.gnu.org/news/atom.php?group=gnuboot) feed).

- It is also available in [Planet GNU](https://planet.gnu.org/) among
  the news of other GNU packages.

Also note that other GNU packages typically announce their news in the
same places, so if you want to follow the news of other GNU packages
they also typically have mailing lists for announcements, they are
also usually available on Planet GNU and also have an atom feed
available through Savannah.