#!/usr/bin/env bash
# Copyright (C) 2023 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

. resources/scripts/misc/sysexits.sh

progname="release"

set -e

topdir="$(realpath "$(dirname "$(dirname "$(dirname "$(dirname "$0")")")")")"

usage()
{
    progname="$1"

    printf "Usage: %s [options]\n\n" "${progname}"
    printf "Available options:\n"
    printf "\t-h, --help\n"
    printf "\t\tDisplay this help and exit.\n"
}

build_website()
{
    version="$("${topdir}"/resources/git/git describe --tags HEAD)"

    cd website
    ./autogen.sh
    ./configure --disable-guix --disable-lighttpd
    make website.tar.gz

    # Release website source and transformed files
    install -d ../release
    install -m 644 website.tar.gz ../release/gnuboot-website-"${version}".tar.gz

    # Relase untitled source code
    "${topdir}"/resources/git/git \
        -C untitled \
        bundle create ../../release/untitled-"${version}".bundle HEAD
}

if [ $# -eq 1 ] && { [ "$1" = "-h" ] || [ "$1" == "--help" ] ;} ; then
    usage "${progname}"
    exit 0
elif [ $# -eq 0 ] ; then
    build_website
else
    usage "${progname}"
    exit ${EX_USAGE}
fi
