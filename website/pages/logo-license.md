---
title: GNU Boot logo license
...

The boot logo for GNUBoot is copyright 2023 Jason Self and released under
[the CC-0 license](https://creativecommons.org/publicdomain/zero/1.0/legalcode).

The grey backgrounds with it were made by Adrien 'neox' Bourmault in
2023, and are also released under [the CC-0
license](https://creativecommons.org/publicdomain/zero/1.0/legalcode).

The logos can be found in the
[resources/grub/background/](https://git.savannah.gnu.org/cgit/gnuboot.git/tree/resources/grub/background)
directory in the GNU Boot source code.