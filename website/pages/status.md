---
title: GNU Boot status
...

GNU Boot 0.1 RC5 status
=======================

+------------------+--------------+--------------+--------------+--------------+
| Vendor, product  | GRUB with    | GRUB with    | SeaBIOS with | SeaBIOS with |
|                  | high         | text-only    | high         | text-only    |
|                  | resolution   | low          | resolution   | low          |
|                  | graphics     | resolution   | graphics     | resolution   |
+------------------+--------------+--------------+--------------+--------------+
| Acer, G43T-AM3   | Missing      | Untested     | Missing      | Untested     |
|                  | images       |              | images       |              |
+------------------+--------------+--------------+--------------+--------------+
| Apple,           | Untested     | Untested     | Untested     | Untested     |
| MacBook 1.1      |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Apple,           | Untested     | Untested     | Untested     | Untested     |
| MacBook 2.1      |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Apple, iMac 5,2  | Untested     | Untested     | Untested     | Untested     |
+------------------+--------------+--------------+--------------+--------------+
| Asus,            | Missing      | Missing      | Missing      | Missing      |
| Chromebook C201  | images       | images       | images       | images       |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KCMA-D8    | Missing      | Untested     | Missing      | Untested     |
| with ECC RAM     | images       |              | images       |              |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KCMA-D8    | Missing      | Untested     | Missing      | Untested     |
| with non-ECC     | images       |              | images       |              |
| RAM              |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KFSN4-DRE  | Missing      | Missing      | Missing      | Untested     |
|                  | images       | images       | images       |              |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KGPE-D16   | Missing      | Tested       | Missing      | Untested     |
| with ECC RAM     | images       |              | images       |              |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KGPE-D16   | Missing      | Untested     | Missing      | Untested     |
| with non-ECC RAM | images       |              | images       |              |
+------------------+--------------+--------------+--------------+--------------+
| Gigabyte,        | Missing      | Missing      | Missing      | Fail: #66463 |
| D945GCLF2D       | images       | images       | images       |              |
+------------------+--------------+--------------+--------------+--------------+
| Gigabyte,        | Missing      | Untested     | Missing      | Untested     |
| GA-G41M-ES2L     | images       |              | images       |              |
+------------------+--------------+--------------+--------------+--------------+
| Intel, D410PT    | Untested     | Untested     | Untested     | Untested     |
+------------------+--------------+--------------+--------------+--------------+
| Intel, D510MO    | Missing      | Untested     | Missing      | Untested     |
|                  | images       |              | images       |              |
+------------------+--------------+--------------+--------------+--------------+
| Intel, D945GCLF  | Missing      | Missing      | Missing      | Untested     |
|                  | images       | images       | images       |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Tested       | Untested     | Untested     | Untested     |
| ThinkPad R400    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Untested     | Untested     |
| ThinkPad R500    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Untested     | Untested     |
| ThinkPad T400    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Untested     | Untested     |
| ThinkPad T400S   |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Tested       | Untested     | Untested     | Untested     |
| ThinkPad T500    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Untested     | Untested     |
| ThinkPad T60     |              |              |              |              |
| with intel GPU   |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Untested     | Untested     |
| ThinkPad W500    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Tested       | Untested     | Untested     | Untested     |
| ThinkPad X200    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Untested     | Untested     |
|  ThinkPad X200S  |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Untested     | Untested     |
| ThinkPad X200T   |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Untested     | Untested     |
| ThinkPad X301    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Untested     | Untested     |
| ThinkPad X60     |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Untested     | Untested     |
| ThinkPad X60T    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Untested     | Untested     |
| ThinkPad X60s    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Libiquity,       | Untested     | Untested     | Untested     | Untested     |
| Taurinus X200    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Qemu,            | Untested     | Untested     | Untested     | Untested     |
| PC (i440FX)      |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | Untested     | Untested     | Untested     | Untested     |
| D16              |              |              |              |              |
| with ECC RAM     |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | Untested     | Untested     | Untested     | Untested     |
| D16              |              |              |              |              |
| with non-ECC RAM |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | Untested     | Untested     | Untested     | Untested     |
| T400             |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | Untested     | Untested     | Untested     | Untested     |
| T400s            |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | Untested     | Untested     | Untested     | Untested     |
| T500             |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | Untested     | Untested     | Untested     | Untested     |
| X200             |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | Untested     | Untested     | Untested     | Untested     |
| X200s            |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | Untested     | Untested     | Untested     | Untested     |
| X200 Tablet      |              |              |              |              |
| (X200T)          |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings,         | Missing      | Untested     | Missing      | Untested     |
| ASUS KCMA D8     | images       |              | images       |              |
| mainboard and    |              |              |              |              |
| workstation      |              |              |              |              |
| with ECC RAM     |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings,         | Missing      | Untested     | Missing      | Untested     |
| ASUS KCMA D8     | images       |              | images       |              |
| mainboard and    |              |              |              |              |
| workstation with |              |              |              |              |
| non-ECC RAM      |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings,         | Missing      | Untested     | Missing      | Untested     |
| ASUS KGPE D16    | images       |              | images       |              |
| mainboard with   |              |              |              |              |
| ECC RAM          |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings,         | Missing      | Untested     | Missing      | Untested     |
| ASUS KGPE D16    | images       |              | images       |              |
| mainboard with   |              |              |              |              |
| non-ECC RAM      |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings, X200    | Untested     | Untested     | Untested     | Untested     |
+------------------+--------------+--------------+--------------+--------------+

Stability:
----------

* Untested: The GNU Boot maintainers are not aware of anybody who
  tested GNU Boot 0.1 RC5 on that computer. If you have GNU Boot on
  this computer please report if it works or not (for instance by
  opening a bug report).

* Fail: #<bug number>: Someone tested GNU Boot 0.1 RC5 on that
  computer and it either doesn't boot at all, can't boot any fully
  free GNU/Linux distribution. It should also be confirmed that this
  is a software issue and not a problem with the hardware like broken
  RAM modules. The issue should also be described in a bug report that
  is available at https://savannah.gnu.org/bugs/index.php?<bug number>
  (this is the GNU Boot bug tracker).

* Tested: Someone tested GNU Boot 0.1 RC5 on that computer and
  reported to the GNU Boot that it at least booted fine.

* Daily users: Some people contacted the GNU Boot maintainers and
  volunteered to send bug reports if installing GNU Boot made the
  computer unusable or very hard to use (like a memory corruption that
  makes it impossible to boot certain GNU/Linux distributions or
  crashes the computer randomly). If you want to help GNU Boot
  with that, please contact the maintainers through a bug report.

GNU Boot 0.1 RC4 status
=======================

+------------------+--------------+--------------+--------------+--------------+
| Vendor, product  | GRUB with    | GRUB with    | SeaBIOS with | SeaBIOS with |
|                  | high         | text-only    | high         | text-only    |
|                  | resolution   | low          | resolution   | low          |
|                  | graphics     | resolution   | graphics     | resolution   |
+------------------+--------------+--------------+--------------+--------------+
| Acer, G43T-AM3   | Missing      | Untested     | Missing      | Fail: #66487 |
|                  | images       |              | images       |              |
+------------------+--------------+--------------+--------------+--------------+
| Apple,           | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| MacBook 1.1      |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Apple,           | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| MacBook 2.1      |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Apple, iMac 5,2  | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
+------------------+--------------+--------------+--------------+--------------+
| Asus,            | Missing      | Missing      | Missing      | Missing      |
| Chromebook C201  | images       | images       | images       | images       |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KCMA-D8    | Missing      | Untested     | Missing      | Untested     |
| with ECC RAM     | images       |              | images       |              |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KCMA-D8    | Missing      | Untested     | Missing      | Untested     |
| with non-ECC RAM | images       |              | images       |              |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KFSN4-DRE  | Missing      | Missing      | Missing      | Fail: #66487 |
|                  | images       | images       | images       |              |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KGPE-D16   | Missing      | Tested       | Missing      | Fail: #66487 |
| with ECC RAM     | images       |              | images       |              |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KGPE-D16   | Missing      | Untested     | Missing      | Fail: #66487 |
| with non-ECC RAM | images       |              | images       |              |
+------------------+--------------+--------------+--------------+--------------+
| Gigabyte,        | Missing      | Missing      | Missing      | Fail: #66463 |
| D945GCLF2D       | images       | images       | images       | Fail: #66487 |
+------------------+--------------+--------------+--------------+--------------+
| Gigabyte,        | Missing      | Untested     | Missing      | Fail: #66487 |
| GA-G41M-ES2L     | images       |              | images       |              |
+------------------+--------------+--------------+--------------+--------------+
| Intel, D410PT    | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
+------------------+--------------+--------------+--------------+--------------+
| Intel, D510MO    | Missing      | Untested     | Missing      | Fail: #66487 |
|                  | images       |              | images       |              |
+------------------+--------------+--------------+--------------+--------------+
| Intel, D945GCLF  | Missing      | Missing      | Missing      | Fail: #66487 |
|                  | images       | images       | images       |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Tested       | Untested     | Fail: #66487 | Fail: #66487 |
| ThinkPad R400    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| ThinkPad R500    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| ThinkPad T400    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| ThinkPad T400S   |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Tested       | Untested     | Fail: #66487 | Fail: #66487 |
| ThinkPad T500    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| ThinkPad T60     |              |              |              |              |
| with intel GPU   |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| ThinkPad W500    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| ThinkPad X200    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
|  ThinkPad X200S  |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| ThinkPad X200T   |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| ThinkPad X301    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| ThinkPad X60     |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| ThinkPad X60T    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| ThinkPad X60s    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Libiquity,       | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| Taurinus X200    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Qemu,            | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| PC (i440FX)      |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| D16 with ECC RAM |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| D16 with non-ECC |              |              |              |              |
| RAM              |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| T400             |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| T400s            |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| T500             |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| X200             |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| X200s            |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
| X200 Tablet      |              |              |              |              |
| (X200T)          |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings,         | Missing      | Untested     | Missing      | Untested     |
| ASUS KCMA D8     | images       |              | images       |              |
| mainboard and    |              |              |              |              |
| workstation      |              |              |              |              |
| with ECC RAM     |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings,         | Missing      | Untested     | Missing      | Untested     |
| ASUS KCMA D8     | images       |              | images       |              |
| mainboard and    |              |              |              |              |
| workstation with |              |              |              |              |
| non-ECC RAM      |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings,         | Missing      | Tested       | Missing      | Fail: #66487 |
| ASUS KGPE D16    | images       |              | images       |              |
| mainboard with   |              |              |              |              |
| ECC RAM          |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings,         | Missing      | Untested     | Missing      | Fail: #66487 |
| ASUS KGPE D16    | images       |              | images       |              |
| mainboard with   |              |              |              |              |
| non-ECC RAM      |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings, X200    | Untested     | Untested     | Fail: #66487 | Fail: #66487 |
+------------------+--------------+--------------+--------------+--------------+

See the status of GNU Boot 0.1 RC5 above for the meaning of the
various fields.

GNU Boot 0.1 RC3 status
=======================

+---------------+--------------------+--------------+-------------+------------+
| Vendor        | Product            | Stability    |Installation |Upgrade     |
|               |                    |              |instructions |instructions|
+---------------+--------------------+--------------+-------------+------------+
| Acer          | G43T-AM3           | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Apple         | MacBook 1.1        | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Apple         | MacBook 2.1        | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Apple         | iMac 5,2           | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Asus          | Chromebook C201    | Missing      | Missing     | Missing    |
|               |                    | images       | images      | images     |
+---------------+--------------------+--------------+-------------+------------+
| Asus          | KCMA-D8            | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Asus          | KFSN4-DRE          | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Asus          | KGPE-D16           | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Gigabyte      | D945GCLF2D         | Fail: #66463 | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Gigabyte      | GA-G41M-ES2L       | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Intel         | D410PT             | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Intel         | D510MO             | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Intel         | D945GCLF           | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad R400      | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad R500      | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad T400      | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad T400S     | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad T500      | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad T60       | Untested     | Untested    | Untested   |
|               | with intel GPU     |              |             |            |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad W500      | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X200      | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X200S     | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X200T     | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X301      | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X60       | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X60T      | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X60s      | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Libiquity     | Taurinus X200      | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Qemu          | PC (i440FX)        | Untested     | Missing     | Missing    |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | D16                | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | T400               | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | T400s              | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | T500               | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | X200               | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | X200s              | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | X200 Tablet        | Untested     | Untested    | Untested   |
|               | (X200T)            |              |             |            |
+---------------+--------------------+--------------+-------------+------------+
| Vikings       | ASUS KCMA D8       | Untested     | Untested    | Untested   |
|               | mainboard          |              |             |            |
|               | and workstation    |              |             |            |
+---------------+--------------------+--------------+-------------+------------+
| Vikings       | ASUS KGPE D16      | Tested       | Untested    | Untested   |
|               | mainboard          |              |             |            |
+---------------+--------------------+--------------+-------------+------------+
| Vikings       | X200               | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+

See the status of GNU Boot 0.1 RC5 above for the meaning of the
various fields.

GNU Boot 0.1 RC2 status
=======================

There are no changes affecting images between GNU Boot 0.1 RC2 and GNU
Boot 0.1 RC3 so it's not worth testing the RC2. It's better to test
the RC3 directly. Yet GNU Boot maintainers tested some computers on
the RC2 to reduce the risk of testers.

+---------------+--------------------+--------------+-------------+------------+
| Vendor        | Product            | Stability    | Install     | Upgrade    |
|               |                    |              | information | information|
+---------------+--------------------+--------------+-------------+------------+
| Apple         | MacBook 1.1        | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Apple         | MacBook 2.1        | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Apple         | iMac 5,2           | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Asus          | Chromebook C201    | Missing      | Missing     | Missing    |
|               |                    | images       | images      | images     |
+---------------+--------------------+--------------+-------------+------------+
| Asus          | KCMA-D8            | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Asus          | KFSN4-DRE          | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Asus          | KGPE-D16           | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Gigabyte      | D945GCLF2D         | Fail: #66463 | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Gigabyte      | GA-G41M-ES2L       | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Intel         | D410PT             | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Intel         | D510MO             | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Intel         | D945GCLF           | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad R400      | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad R500      | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad T400      | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad T400S     | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad T500      | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad T60       | Tested       | Untested    | Untested   |
|               | with intel GPU     |              |             |            |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad W500      | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X200      | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X200S     | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X200T     | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X301      | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X60       | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X60T      | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X60s      | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Libiquity     | Taurinus X200      | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Qemu          | PC (i440FX)        | Tested       | Missing     | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | D16                | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | T400               | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | T400s              | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | T500               | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | X200               | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | X200s              | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | X200 Tablet        | Untested     | Untested    | Untested   |
|               | (X200T)            |              |             |            |
+---------------+--------------------+--------------+-------------+------------+
| Vikings       | ASUS KCMA D8       | Untested     | Untested    | Untested   |
|               | mainboard          |              |             |            |
|               | and workstation    |              |             |            |
+---------------+--------------------+--------------+-------------+------------+
| Vikings       | ASUS KGPE D16      | Untested     | Untested    | Untested   |
|               | mainboard          |              |             |            |
+---------------+--------------------+--------------+-------------+------------+
| Vikings       | X200               | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+

See the status of GNU Boot 0.1 RC5 above for the meaning of the
various fields.

GNU Boot 0.1 RC1 status
=======================

+---------------+--------------------+--------------+-------------+------------+
| Vendor        | Product            | Stability    | Install     | Upgrade    |
|               |                    |              | information | information|
+---------------+--------------------+--------------+-------------+------------+
| Apple         | MacBook 1.1        | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Apple         | MacBook 2.1        | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Apple         | iMac 5,2           | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Asus          | Chromebook C201    | Missing      | Missing     | Missing    |
|               |                    | images       | images      | images     |
+---------------+--------------------+--------------+-------------+------------+
| Asus          | KCMA-D8            | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Asus          | KFSN4-DRE          | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Asus          | KGPE-D16           | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Gigabyte      | D945GCLF2D         | Fail: #66463 | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Gigabyte      | GA-G41M-ES2L       | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Intel         | D410PT             | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Intel         | D510MO             | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Intel         | D945GCLF           | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad R400      | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad R500      | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad T400      | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad T400S     | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad T500      | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad T60       | Tested       | Untested    | Untested   |
|               | with intel GPU     |              |             |            |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad W500      | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X200      | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X200S     | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X200T     | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X301      | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X60       | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X60T      | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Lenovo        | ThinkPad X60s      | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Libiquity     | Taurinus X200      | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | D16                | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | T400               | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | T400s              | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | T500               | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | X200               | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | X200s              | Untested     | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+
| Technoethical | X200 Tablet        | Untested     | Untested    | Untested   |
|               | (X200T)            |              |             |            |
+---------------+--------------------+--------------+-------------+------------+
| Vikings       | ASUS KCMA D8       | Untested     | Untested    | Untested   |
|               | mainboard          |              |             |            |
|               | and workstation    |              |             |            |
+---------------+--------------------+--------------+-------------+------------+
| Vikings       | ASUS KGPE D16      | Tested       | Untested    | Untested   |
|               | mainboard          |              |             |            |
+---------------+--------------------+--------------+-------------+------------+
| Vikings       | X200               | Tested       | Untested    | Untested   |
+---------------+--------------------+--------------+-------------+------------+

See the status of GNU Boot 0.1 RC5 above for the meaning of the
various fields.

Upstream status
===============

+----------+-----------------+---------------------------+---------------------+
| Vendor   | Product         | Coreboot status           | Coreboot GPU driver |
+----------+-----------------+---------------------------+---------------------+
| Apple    | MacBook 1.1     | Maintained                | old GPU driver      |
+----------+-----------------+---------------------------+---------------------+
| Apple    | MacBook 2.1     | Maintained                | old GPU driver      |
+----------+-----------------+---------------------------+---------------------+
| Apple    | iMac 5,2        | Maintained                | old GPU driver      |
+----------+-----------------+---------------------------+---------------------+
| Asus     | Chromebook C201 | Unmaintained              |                     |
+----------+-----------------+---------------------------+---------------------+
| Asus     | KCMA-D8         | Removed                   |                     |
+----------+-----------------+---------------------------+---------------------+
| Asus     | KFSN4-DRE       | Removed                   |                     |
+----------+-----------------+---------------------------+---------------------+
| Asus     | KGPE-D16        | Removed                   |                     |
+----------+-----------------+---------------------------+---------------------+
| Gigabyte | D945GCLF2D      | Similar to Intel D945GCLF |                     |
+----------+-----------------+---------------------------+---------------------+
| Gigabyte | GA-G41M-ES2L    | Unmaintained              |                     |
+----------+-----------------+---------------------------+---------------------+
| Intel    | D410PT          | Unmaintained              |                     |
+----------+-----------------+---------------------------+---------------------+
| Intel    | D510MO          | Unmaintained              |                     |
+----------+-----------------+---------------------------+---------------------+
| Intel    | D945GCLF        | Unmaintained              | new GPU driver      |
+----------+-----------------+---------------------------+---------------------+
| Lenovo   | ThinkPad R400   | Maintained                | new GPU driver      |
+----------+-----------------+---------------------------+---------------------+
| Lenovo   | ThinkPad R500   | Maintained                | new GPU driver      |
+----------+-----------------+---------------------------+---------------------+
| Lenovo   | ThinkPad T400   | Maintained                | new GPU driver      |
+----------+-----------------+---------------------------+---------------------+
| Lenovo   | ThinkPad T400s  | Similar to ThinkPad T400  | new GPU driver      |
+----------+-----------------+---------------------------+---------------------+
| Lenovo   | ThinkPad T500   | Maintained                | new GPU driver      |
+----------+-----------------+---------------------------+---------------------+
| Lenovo   | ThinkPad T60    | Maintained                | old GPU driver      |
|          | with intel GPU  |                           |                     |
+----------+-----------------+---------------------------+---------------------+
| Lenovo   | ThinkPad W500   | Maintained                | new GPU driver      |
+----------+-----------------+---------------------------+---------------------+
| Lenovo   | ThinkPad X200   | Maintained                | new GPU driver      |
+----------+-----------------+---------------------------+---------------------+
| Lenovo   | ThinkPad X200s  | Maintained                | new GPU driver      |
+----------+-----------------+---------------------------+---------------------+
| Lenovo   | ThinkPad X200T  | Maintained                | new GPU driver      |
+----------+-----------------+---------------------------+---------------------+
| Lenovo   | ThinkPad X301   | Maintained                | new GPU driver      |
+----------+-----------------+---------------------------+---------------------+
| Lenovo   | ThinkPad X60    | Maintained                | old GPU driver      |
+----------+-----------------+---------------------------+---------------------+
| Lenovo   | ThinkPad X60T   | Maintained                | old GPU driver      |
+----------+-----------------+---------------------------+---------------------+
| Lenovo   | ThinkPad X60s   | Maintained                | old GPU driver      |
+----------+-----------------+---------------------------+---------------------+

Coreboot status:
----------------

* Maintained / Unmaintained: GNU Boot depends on Coreboot to support
  specific computers. So if Coreboot stops supporting a computer that
  GNU Boot supports, then it will become harder and harder to support
  that computer in GNU Boot over time and at some point GNU Boot will
  likely stop supporting it unless someone adds back that computer in
  Coreboot. And if a computer has no maintainer in Coreboot it
  increases the likelihood that it will be removed at some point.

* Removed: Coreboot already removed the support for this computer.

* Similar to <another computer>: There is no official support for this
  computer in Coreboot but using the code from the other computer
  mentioned works.

Coreboot GPU driver:
--------------------

* old GPU driver / new GPU driver: Some computers uses an old GPU
  driver written in C that could have some stability issues that were
  fixed in the new GPU driver written in ADA. When the field is blank,
  it means that we didn't look at which GPU driver was used or its
  stability.

Upstream versions used in GNU Boot 0.1 RC3
==========================================

+------------------+---------+------------+------------------------------------+
| Upstream project | Version | Needs      | Computers using it                 |
|                  |         | deblobbing |                                    |
+------------------+---------+------------+------------------------------------+
| Coreboot         |   4.11+ | Yes        | Asus: KCMA-D8, KFSN4, KGPE-D16     |
+------------------+---------+------------+------------------------------------+
| Coreboot         |   4.15+ | Yes        | All but Asus KCMA-D8, KFSN4,       |
|                  |         |            | KGPE-D16.                          |
+------------------+---------+------------+------------------------------------+
| GRUB             |   2.06+ | No         | All supported computers            |
+------------------+---------+------------+------------------------------------+
| Memtest86+       |         |            | All but Gigabyte GA-G41M-ES2L and  |
| for Coreboot     |   v002+ | No         | Intel D510MO                       |
+------------------+---------+------------+------------------------------------+
| SeaBIOS          | 1.14.0+ | No         | All supported computers            |
+------------------+---------+------------+------------------------------------+

Reproducible builds
===================

This tracks the status of reproducible builds in GNU Boot.

See [Building GNU Boot from source
chapter](https://www.gnu.org/software/gnuboot/web/manual/gnuboot.html#Building-GNU-Boot-from-source)
in the [GNU Boot
manual](http://localhost:8086/software/gnuboot/web/docs/#manuals) for
more details about this issue.

Also note that the table below is meant for the [GNU Boot
images](http://localhost:8086/software/gnuboot/web/manual/gnuboot.html#index-flash-images)
which is what gets installed and what interest more users.

+------------------+------------------------------+--------------+-------------+
| Provenance       | Path inside the image        | Reproducible | Usage       |
+------------------+------------------------------+--------------+-------------+
| Coreboot         | * CBFS headers               | Unknown      | all images  |
+------------------+------------------------------+--------------+-------------+
| Coreboot         | * /bootblock                 | Unknown      | all images  |
+------------------+------------------------------+--------------+-------------+
| Coreboot         | * /build_info                | Unknown      | all images  |
+------------------+------------------------------+--------------+-------------+
| Coreboot         | * /cmos.default              | Unknown      | all but     |
|                  |                              |              | 16MB macbook|
|                  |                              |              | images      |
+------------------+------------------------------+--------------+-------------+
| Coreboot         | * /cmos_layout.bin           | Unknown      | all images  |
+------------------+------------------------------+--------------+-------------+
| Coreboot         | * /config                    | Unknown      | all images  |
+------------------+------------------------------+--------------+-------------+
| Coreboot         | * /data_ccfl.vbt             | N/A: not     | all images  |
|                  |                              | built        |             |
+------------------+------------------------------+--------------+-------------+
| Coreboot         | * /data_led.vbt              | N/A: not     | all images  |
|                  |                              | built        |             |
+------------------+------------------------------+--------------+-------------+
| Coreboot         | * /vbt.bin                   | Unknown      | computers   |
|                  |                              |              | with Intel  |
|                  |                              |              | GPU         |
+------------------+------------------------------+--------------+-------------+
| Coreboot         | * /fallback/dsdt.aml         | Unknown      | all images  |
+------------------+------------------------------+--------------+-------------+
| Coreboot         | * /fallback/postcar          | Unknown      | all but     |
|                  |                              |              | KGPE-D16 and|
|                  |                              |              | KCMA-D8     |
+------------------+------------------------------+--------------+-------------+
| Coreboot         | * /fallback/ramstage         | Unknown      | all images  |
+------------------+------------------------------+--------------+-------------+
| Coreboot         | * /fallback/romstage         | Unknown      | all images  |
+------------------+------------------------------+--------------+-------------+
| Coreboot         | * /revision                  | Unknown      | all images  |
+------------------+------------------------------+--------------+-------------+
| Coreboot         | * /rt8168-macaddress         | Unknown      | all images  |
+------------------+------------------------------+--------------+-------------+
| GRUB             | * /fallback/payload          | Unknown      | GRUB images |
+------------------+------------------------------+--------------+-------------+
| GRUB             | * /img/grub2                 | Unknown      | SeaBIOS     |
|                  |                              |              | images      |
+------------------+------------------------------+--------------+-------------+
| Memtest86+       | * /img/memtest               | Unknown      | more than   |
| for Coreboot     |                              |              | half of the |
|                  |                              |              | images      |
+------------------+------------------------------+--------------+-------------+
| SeaBIOS          | * /fallback/payload          | Unknown      | SeaBIOS     |
|                  |                              |              | images      |
+------------------+------------------------------+--------------+-------------+
| SeaBIOS          | * /vgaroms/seavgabios.bin    | Unknown      | all images  |
+------------------+------------------------------+--------------+-------------+
| ich9gen          | * Intel flash descriptor     | Yes: verified| GM45        |
|                  |                              | by checksum  | ThinkPads   |
+------------------+------------------------------+--------------+-------------+
| ich9gen          | * Intel Gigabit ethernet     | Yes: verified| GM45        |
|                  |   partition                  | by checksum  | ThinkPads   |
+------------------+------------------------------+--------------+-------------+
| GNU Boot         | * /background.png            | Unknown      | all but     |
| build system     |                              |              | d945gclf and|
|                  |                              |              | kfsn4-dre   |
+------------------+------------------------------+--------------+-------------+
| GNU Boot         | * /etc/only-load-option-roms | Unknown      | all images  |
| build system     |                              |              |             |
+------------------+------------------------------+--------------+-------------+
| GNU Boot         | * /etc/optionroms-checksum   | Unknown      | all images  |
| build system     |                              |              |             |
+------------------+------------------------------+--------------+-------------+
| GNU Boot         | * /etc/pci-optionrom-exec    | Unknown      | all images  |
| build system     |                              |              |             |
+------------------+------------------------------+--------------+-------------+
| GNU Boot         | * /etc/ps2-keyboard-spinup   | Unknown      | all images  |
| build system     |                              |              |             |
+------------------+------------------------------+--------------+-------------+
| GNU Boot         | * /grub.cfg                  | Unknown      | all but     |
| build system     |                              |              | d945gclf and|
|                  |                              |              | kfsn4-dre   |
+------------------+------------------------------+--------------+-------------+
| GNU Boot         | * /grubtest.cfg              | Unknown      | kfsn4-dre   |
| build system     |                              |              |             |
+------------------+------------------------------+--------------+-------------+
| GNU Boot         |* pci1000,0072.rom            | N/A: empty   | KGPE-D16    |
| build system     |                              | file         | and KCMA-D8 |
+------------------+------------------------------+--------------+-------------+
| GNU Boot         |* pci1000,3050.rom            | N/A: empty   | KGPE-D16    |
| build system     |                              | file         | and KCMA-D8 |
+------------------+------------------------------+--------------+-------------+

Also note that not everything is in the table above:

* The gnuboot-trisquel-grub.img file, which is produced by GNU Boot
  and that is used during the automatic tests is reproducible. It can
  be used as an example to make other components built with Guix
  reproducible.

Limitations
===========

+------------------+----------------------------------------+---------------------------------------------------------+
| Component        | Limitations                            | Bug number                                              |
+------------------+----------------------------------------+---------------------------------------------------------+
| SeaBIOS images   | - No VGA support                       | [#65922](https://savannah.gnu.org/bugs/index.php?65922) |
|                  |   => Require to edit /etc/default/grub |                                                         |
+------------------+----------------------------------------+---------------------------------------------------------+
| Install          | - Only for technical people            |                                                         |
| instructions     |                                        |                                                         |
+------------------+----------------------------------------+---------------------------------------------------------+
| Upgrade          | - Only for technical people            |                                                         |
| instructions     |                                        |                                                         |
+------------------+----------------------------------------+---------------------------------------------------------+
| Website          | - No proper manual                     |                                                         |
+------------------+----------------------------------------+---------------------------------------------------------+
| Website          | - Many unreviewed pages                |                                                         |
+------------------+----------------------------------------+---------------------------------------------------------+
