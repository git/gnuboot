---
title: Intel D510MO and D410PT desktop boards
x-unreviewed: true
...

This is a desktop board using intel hardware (circa \~2009, ICH7
southbridge, similar performance-wise to the Libreboot X200. It can make
for quite a nifty desktop. Powered by libreboot.

NOTE: D410PT is another name and it's the same board. Flash the exact same
ROM and it should work.

NOTE: This board has a working framebuffer in Grub, but in GNU+Linux in
native resolution the display is unusable due to some raminit issues.
This board can however be used for building a headless server.

Flashing instructions can be found at
[../install/d510mo.md](../install/d510mo.md)
