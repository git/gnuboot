---
title: Estado de GNU Boot
...

Estado de GNU Boot 0.1 RC5
==========================

+------------------+--------------+--------------+--------------+--------------+
| Vendedor,        | GRUB con     | GRUB con     | SeaBIOS con  | SeaBIOS con  |
| Producto         | gráficos     | texto        | gráficos     | texto        |
|                  | alta         | baja         | alta         | baja         |
|                  | resolución   | resolución   | resolución   | resolución   |
+------------------+--------------+--------------+--------------+--------------+
| Acer, G43T-AM3   | Faltan       | No probado   | Faltan       | No probado   |
|                  | imágenes     |              | imágenes     |              |
+------------------+--------------+--------------+--------------+--------------+
| Apple,           | No probado   | No probado   | No probado   | No probado   |
+------------------+--------------+--------------+--------------+--------------+
| Apple,           | No probado   | No probado   | No probado   | No probado   |
| MacBook 2.1      |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Apple, iMac 5,2  | No probado   | No probado   | No probado   | No probado   |
+------------------+--------------+--------------+--------------+--------------+
| Asus,            | Faltan       | Faltan       | Faltan       | Faltan       |
| Chromebook C201  | imágenes     | imágenes     | imágenes     | imágenes     |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KCMA-D8    | Faltan       | No probado   | Faltan       | No probado   |
| con RAM ECC      | imágenes     |              | imágenes     |              |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KCMA-D8    | Faltan       | No probado   | Faltan       | No probado   |
| con RAM no-ECC   | imágenes     |              | imágenes     |              |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KFSN4-DRE  | Faltan       | Faltan       | Faltan       | No probado   |
|                  | imágenes     | imágenes     | imágenes     |              |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KGPE-D16   | Faltan       | Probado      | Faltan       | No probado   |
| con RAM ECC      | imágenes     |              | imágenes     |              |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KGPE-D16   | Faltan       | No probado   | Faltan       | No probado   |
| con RAM no-ECC   | imágenes     |              | imágenes     |              |
+------------------+--------------+--------------+--------------+--------------+
| Gigabyte,        | Faltan       | Faltan       | Faltan       | Fallado:     |
| D945GCLF2D       | imágenes     | imágenes     | imágenes     | #66463       |
+------------------+--------------+--------------+--------------+--------------+
| Gigabyte,        | No probado   | No probado   | No probado   | No probado   |
| GA-G41M-ES2L     |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Intel, D410PT    | No probado   | No probado   | No probado   | No probado   |
+------------------+--------------+--------------+--------------+--------------+
| Intel, D510MO    | No probado   | No probado   | No probado   | No probado   |
+------------------+--------------+--------------+--------------+--------------+
| Intel, D945GCLF  | Faltan       | Faltan       | Faltan       | No probado   |
|                  | imágenes     | imágenes     | imágenes     |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Probado      | No probado   | No probado   | No probado   |
| ThinkPad R400    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | No probado   | No probado   |
| ThinkPad R500    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | No probado   | No probado   |
| ThinkPad T400    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | No probado   | No probado   |
| ThinkPad T400S   |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Probado      | No probado   | No probado   | No probado   |
| ThinkPad T500    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | No probado   | No probado   |
| ThinkPad T60     |              |              |              |              |
| with intel GPU   |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | No probado   | No probado   |
| ThinkPad W500    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Probado      | No probado   | No probado   | No probado   |
| ThinkPad X200    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | No probado   | No probado   |
|  ThinkPad X200S  |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | No probado   | No probado   |
| ThinkPad X200T   |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | No probado   | No probado   |
| ThinkPad X301    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | No probado   | No probado   |
| ThinkPad X60     |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | No probado   | No probado   |
| ThinkPad X60T    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | No probado   | No probado   |
|  ThinkPad X60s   |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Libiquity,       | No probado   | No probado   | No probado   | No probado   |
| Taurinus X200    |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Qemu,            | No probado   | No probado   | No probado   | No probado   |
| PC (i440FX)      |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | No probado   | No probado   | No probado   | No probado   |
| D16 con RAM ECC  |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | No probado   | No probado   | No probado   | No probado   |
| D16 con RAM      |              |              |              |              |
| no-ECC           |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | No probado   | No probado   | No probado   | No probado   |
| T400             |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | No probado   | No probado   | No probado   | No probado   |
| T400s            |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | No probado   | No probado   | No probado   | No probado   |
| T500             |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | No probado   | No probado   | No probado   | No probado   |
| X200             |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | No probado   | No probado   | No probado   | No probado   |
| X200s            |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | No probado   | No probado   | No probado   | No probado   |
| X200 Tablet      |              |              |              |              |
| (X200T)          |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings,         | Faltan       | No probado   | Faltan       | No probado   |
| ASUS KCMA D8     | imágenes     |              | imágenes     |              |
| mainboard and    |              |              |              |              |
| workstation      |              |              |              |              |
| con RAM ECC      |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings,         | Faltan       | No probado   | Faltan       | No probado   |
| ASUS KCMA D8     | imágenes     |              | imágenes     |              |
| mainboard and    |              |              |              |              |
| workstation      |              |              |              |              |
| con RAM no-ECC   |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings,         | Faltan       | No probado   | Faltan       | No probado   |
| ASUS KGPE D16    | imágenes     |              | imágenes     |              |
| mainboard        |              |              |              |              |
| con RAM ECC      |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings,         | Faltan       | No probado   | Faltan       | No probado   |
| ASUS KGPE D16    | imágenes     |              | imágenes     |              |
| mainboard        |              |              |              |              |
| con RAM no-ECC   |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings, X200    | No probado   | No probado   | No probado   | No probado   |
+------------------+--------------+--------------+--------------+--------------+

Estabilidad:
------------

* No probado: Los mantenedores de GNU Boot no conocen a nadie que haya
  probado GNU Boot 0.1 RC5 en esa computadora. Si tiene GNU Boot en esta
  computadora, informe si funciona o no (por ejemplo, abriendo un informe
  de error).

* Fallado: #<número de error>: Alguien probó GNU Boot 0.1 RC5 en esa
  computadora y no arrancó o no arrancó completamente ninguna
  distribución de GNU con Linux. Se debe confirmar que sea un problema
  software y no hardware hardware como módulos de RAM rotos.
  Se debe describir el problema en un rapporto de incidencias que es
  disponible en https://savannah.gnu.org/bugs/index.php?<número de
  error> (esto es el rastreador de errores de GNU Boot).

* Probado: Alguien probó GNU Boot 0.1 RC5 en esa computadora e informó a
  GNU Boot que al menos arrancó bien.

* Usuarios diarios: algunas personas que se pusieron en contacto con los
  mantenedores de GNU Boot y se ofrecieron como voluntarios para enviar
  informes de errores a cerca de si la instalación de GNU Boot hacía que
  la computadora fuera inutilizable o muy difícil de usar (como una
  corrupción de la memoria que imposibilita el inicio de ciertas
  distribuciones de GNU con Linux o bloquea la computadora
  aleatoriamente). Si desea ayudar a GNU Boot con esto, comuníquese con
  los mantenedores a través de un informe de error.

Instrucciones de instalación:
-----------------------------

* No probado: Nadie probó las instrucciones de instalación de GNU Boot
  para esta computadora.

* Probado: Las instrucciones de instalación funcionaron bien.

* Faltante: No hay instrucciones de instalación para este dispositivo y
  necesitamos ayuda de los contribuyentes para agregarlas.

Estado de GNU Boot 0.1 RC4
==========================

+------------------+--------------+--------------+--------------+--------------+
| Vendedor,        | GRUB con     | GRUB con     | SeaBIOS con  | SeaBIOS con  |
| Producto         | gráficos     | texto        | gráficos     | texto        |
|                  | alta         | baja         | alta         | baja         |
|                  | resolución   | resolución   | resolución   | resolución   |
+------------------+--------------+--------------+--------------+--------------+
| Acer, G43T-AM3   | Faltan       | No probado   | Faltan       | Fallado:     |
|                  | imágenes     |              | imágenes     | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Apple,           | No probado   | No probado   | Fallado:     | Fallado:     |
| MacBook 1.1      |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Apple,           | No probado   | No probado   | Fallado:     | Fallado:     |
| MacBook 2.1      |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Apple, iMac 5,2  | No probado   | No probado   | Fallado:     | Fallado:     |
|                  |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Asus,            | Faltan       | Faltan       | Faltan       | Faltan       |
| Chromebook C201  | imágenes     | imágenes     | imágenes     | imágenes     |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KCMA-D8    | Faltan       | No probado   | Faltan       | No probado   |
| con RAM ECC      | imágenes     |              | imágenes     |              |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KCMA-D8    | Faltan       | No probado   | Faltan       | No probado   |
| con RAM no-ECC   | imágenes     |              | imágenes     |              |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KFSN4-DRE  | Faltan       | Faltan       | Faltan       | Fallado:     |
|                  | imágenes     | imágenes     | imágenes     | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KGPE-D16   | Faltan       | Probado      | Faltan       | Fallado:     |
| con RAM ECC      | imágenes     |              | imágenes     | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Asus, KGPE-D16   | Faltan       | No probado   | Faltan       | Fallado:     |
| con RAM no-ECC   | imágenes     |              | imágenes     | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Gigabyte,        | Faltan       | Faltan       | Faltan       | Fallado:     |
| D945GCLF2D       | imágenes     | imágenes     | imágenes     |#66463,#66487 |
+------------------+--------------+--------------+--------------+--------------+
| Gigabyte,        | No probado   | No probado   | Fallado:     | Fallado:     |
| GA-G41M-ES2L     |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Intel, D410PT    | No probado   | No probado   | Fallado:     | Fallado:     |
|                  |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Intel, D510MO    | No probado   | No probado   | Fallado:     | Fallado:     |
|                  |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Intel, D945GCLF  | Faltan       | Faltan       | Faltan       | Fallado:     |
|                  | imágenes     | imágenes     | imágenes     | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Probado      | No probado   | Fallado:     | Fallado:     |
| ThinkPad R400    |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | Fallado:     | Fallado:     |
| ThinkPad R500    |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | Fallado:     | Fallado:     |
| ThinkPad T400    |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | Fallado:     | Fallado:     |
| ThinkPad T400S   |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | Probado      | No probado   | Fallado:     | Fallado:     |
| ThinkPad T500    |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | Fallado:     | Fallado:     |
| ThinkPad T60     |              |              | #66487       | #66487       |
| with intel GPU   |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | Fallado:     | Fallado:     |
| ThinkPad W500    |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | Fallado:     | Fallado:     |
| ThinkPad X200    |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | Fallado:     | Fallado:     |
|  ThinkPad X200S  |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | Fallado:     | Fallado:     |
| ThinkPad X200T   |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | Fallado:     | Fallado:     |
| ThinkPad X301    |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | Fallado:     | Fallado:     |
| ThinkPad X60     |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | Fallado:     | Fallado:     |
| ThinkPad X60T    |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Lenovo,          | No probado   | No probado   | Fallado:     | Fallado:     |
|  ThinkPad X60s   |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Libiquity,       | No probado   | No probado   | Fallado:     | Fallado:     |
| Taurinus X200    |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Qemu,            | No probado   | No probado   | Fallado:     | Fallado:     |
| PC (i440FX)      |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | No probado   | No probado   | Fallado:     | Fallado:     |
| D16 con RAM ECC  |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | No probado   | No probado   | Fallado:     | Fallado:     |
| D16 con RAM      |              |              | #66487       | #66487       |
| no-ECC           |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | No probado   | No probado   | Fallado:     | Fallado:     |
| T400             |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | No probado   | No probado   | Fallado:     | Fallado:     |
| T400s            |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | No probado   | No probado   | Fallado:     | Fallado:     |
| T500             |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | No probado   | No probado   | Fallado:     | Fallado:     |
| X200             |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | No probado   | No probado   | Fallado:     | Fallado:     |
| X200s            |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+
| Technoethical,   | No probado   | No probado   | Fallado:     | Fallado:     |
| X200 Tablet      |              |              | #66487       | #66487       |
| (X200T)          |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings,         | Faltan       | No probado   | Faltan       | No probado   |
| ASUS KCMA D8     | imágenes     |              | imágenes     |              |
| mainboard and    |              |              |              |              |
| workstation      |              |              |              |              |
| con RAM ECC      |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings,         | Faltan       | No probado   | Faltan       | No probado   |
| ASUS KCMA D8     | imágenes     |              | imágenes     |              |
| mainboard and    |              |              |              |              |
| workstation      |              |              |              |              |
| con RAM no-ECC   |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings,         | Faltan       | Probado      | Faltan       | Fallado:     |
| ASUS KGPE D16    | imágenes     |              | imágenes     | #66487       |
| mainboard        |              |              |              |              |
| con RAM ECC      |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings,         | Faltan       | No probado   | Faltan       | Fallado:     |
| ASUS KGPE D16    | imágenes     |              | imágenes     | #66487       |
| mainboard        |              |              |              |              |
| con RAM no-ECC   |              |              |              |              |
+------------------+--------------+--------------+--------------+--------------+
| Vikings, X200    | No probado   | No probado   | Fallado:     | Fallado:     |
|                  |              |              | #66487       | #66487       |
+------------------+--------------+--------------+--------------+--------------+

Consulte el estado de GNU Boot 0.1 RC5 arriba para conocer el
significado de los distintos campos.

Estado de GNU Boot 0.1 RC3
==========================

+---------------+----------------+-----------------+-------------+-------------+
| Vendedor      | Producto       | Estabilidad     |Instrucciones|Instrucciones|
|               |                |                 |de           |de mejora.   |
|               |                |                 |instalación  |             |
+---------------+----------------+-----------------+-------------+-------------+
| Acer          | G43T-AM3       | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Apple         | MacBook 1.1    | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Apple         | MacBook 2.1    | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Apple         | iMac 5,2       | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Asus          | Chromebook     | Faltan          | Faltan      | Faltan      |
|               | C201           | imágenes        | imágenes    | imágenes    |
+---------------+----------------+-----------------+-------------+-------------+
| Asus          | KCMA-D8        | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Asus          | KFSN4-DRE      | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Asus          | KGPE-D16       | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Gigabyte      | D945GCLF2D     | Fallado: #66463 | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Gigabyte      | GA-G41M-ES2L   | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Intel         | D410PT         | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Intel         | D510MO         | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Intel         | D945GCLF       | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad R400  | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad R500  | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad T400  | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad T400S | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad T500  | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad T60   | No probado      | No probado  | No probado  |
|               | con GPU intel  |                 |             |             |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad W500  | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X200  | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X200S | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X200T | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X301  | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X60   | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X60T  | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X60s  | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Libiquity     | Taurinus X200  | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Qemu          | PC (i440FX)    | No probado      | Faltante    | Faltante    |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | D16            | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | T400           | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | T400s          | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | T500           | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | X200           | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | X200s          | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | X200 Tablet    | No probado      | No probado  | No probado  |
|               | (X200T)        |                 |             |             |
+---------------+----------------+-----------------+-------------+-------------+
| Vikings       | ASUS KCMA D8   | No probado      | No probado  | No probado  |
|               | and workstation|                 |             |             |
+---------------+----------------+-----------------+-------------+-------------+
| Vikings       | ASUS KGPE D16  | Probado         | No probado  | No probado  |
|               | mainboard      |                 |             |             |
+---------------+----------------+-----------------+-------------+-------------+
| Vikings       | X200           | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+

Consulte el estado de GNU Boot 0.1 RC5 arriba para conocer el
significado de los distintos campos.

Estado de GNU Boot 0.1 RC2
==========================

No hay cambios que afecten a las imágenes entre GNU Boot 0.1 RC2 y GNU
Boot 0.1 RC3, por lo que no vale la pena probar el RC2. Es mejor probar
el RC3 directamente. Sin embargo, los mantenedores de GNU Boot probaron
algunas computadoras en el RC2 para reducir el riesgo de los probadores.

+---------------+----------------+-----------------+-------------+-------------+
| Vendedor      | Producto       | Estabilidad     |Instrucciones|Instrucciones|
|               |                |                 |de           |de mejora.   |
|               |                |                 |instalación  |             |
+---------------+----------------+-----------------+-------------+-------------+
| Apple         | MacBook 1.1    | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Apple         | MacBook 2.1    | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Apple         | iMac 5,2       | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Asus          | Chromebook     | Faltan          | Faltan      | Faltan      |
|               | C201           | imágenes        | imágenes    | imágenes    |
+---------------+----------------+-----------------+-------------+-------------+
| Asus          | KCMA-D8        | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Asus          | KFSN4-DRE      | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Asus          | KGPE-D16       | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Gigabyte      | D945GCLF2D     | Fallado: #66463 | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Gigabyte      | GA-G41M-ES2L   | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Intel         | D410PT         | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Intel         | D510MO         | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Intel         | D945GCLF       | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad R400  | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad R500  | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad T400  | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad T400S | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad T500  | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad T60   | Probado         | No probado  | No probado  |
|               | con GPU intel  |                 |             |             |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad W500  | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X200  | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X200S | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X200T | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X301  | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X60   | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X60T  | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X60s  | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Libiquity     | Taurinus X200  | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Qemu          | PC (i440FX)    | Probado         | Faltante    | Faltante    |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | D16            | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | T400           | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | T400s          | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | T500           | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | X200           | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | X200s          | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | X200 Tablet    | No probado      | No probado  | No probado  |
|               | (X200T)        |                 |             |             |
+---------------+----------------+-----------------+-------------+-------------+
| Vikings       | ASUS KCMA D8   | No probado      | No probado  | No probado  |
|               | and workstation|                 |             |             |
+---------------+----------------+-----------------+-------------+-------------+
| Vikings       | ASUS KGPE D16  | No probado      | No probado  | No probado  |
|               | mainboard      |                 |             |             |
+---------------+----------------+-----------------+-------------+-------------+
| Vikings       | X200           | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+

Consulte el estado de GNU Boot 0.1 RC5 arriba para conocer el
significado de los distintos campos.

Estado de GNU Boot 0.1 RC1
==========================

+---------------+----------------+-----------------+-------------+-------------+
| Vendedor      | Producto       | Estabilidad     |Instrucciones|Instrucciones|
|               |                |                 |de           |de mejora.   |
|               |                |                 |instalación  |             |
+---------------+----------------+-----------------+-------------+-------------+
| Apple         | MacBook 1.1    | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Apple         | MacBook 2.1    | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Apple         | iMac 5,2       | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Asus          | Chromebook     | Faltan          | Faltan      | Faltan      |
|               | C201           | imágenes        | imágenes    | imágenes    |
+---------------+----------------+-----------------+-------------+-------------+
| Asus          | KCMA-D8        | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Asus          | KFSN4-DRE      | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Asus          | KGPE-D16       | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Gigabyte      | D945GCLF2D     | Fallado: #66463 | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Gigabyte      | GA-G41M-ES2L   | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Intel         | D410PT         | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Intel         | D510MO         | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Intel         | D945GCLF       | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad R400  | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad R500  | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad T400  | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad T400S | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad T500  | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad T60   | Probado         | No probado  | No probado  |
|               | con GPU intel  |                 |             |             |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad W500  | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X200  | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X200S | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X200T | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X301  | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X60   | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X60T  | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Lenovo        | ThinkPad X60s  | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Libiquity     | Taurinus X200  | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | D16            | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | T400           | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | T400s          | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | T500           | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | X200           | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | X200s          | No probado      | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+
| Technoethical | X200 Tablet    | No probado      | No probado  | No probado  |
|               | (X200T)        |                 |             |             |
+---------------+----------------+-----------------+-------------+-------------+
| Vikings       | ASUS KCMA D8   | No probado      | No probado  | No probado  |
|               | and workstation|                 |             |             |
+---------------+----------------+-----------------+-------------+-------------+
| Vikings       | ASUS KGPE D16  | Probado         | No probado  | No probado  |
|               | mainboard      |                 |             |             |
+---------------+----------------+-----------------+-------------+-------------+
| Vikings       | X200           | Probado         | No probado  | No probado  |
+---------------+----------------+-----------------+-------------+-------------+

Consulte el estado de GNU Boot 0.1 RC5 arriba para conocer el
significado de los distintos campos.

Estado de "Upstream" (Contracorriente)
======================================

+----------+-----------------+--------------------------+--------------------------------+
| Vendedor | Producto        | Estado de Coreboot       | Controlador de GPU de Coreboot |
+----------+-----------------+--------------------------+--------------------------------+
| Apple    | MacBook 1.1     | Mantenido                | controlador de GPU antiguo     |
+----------+-----------------+--------------------------+--------------------------------+
| Apple    | MacBook 2.1     | Mantenido                | controlador de GPU antiguo     |
+----------+-----------------+--------------------------+--------------------------------+
| Apple    | iMac 5,2        | Mantenido                | controlador de GPU antiguo     |
+----------+-----------------+--------------------------+--------------------------------+
| Asus     | Chromebook C201 | Sin mantenimiento        |                                |
+----------+-----------------+--------------------------+--------------------------------+
| Asus     | KCMA-D8         | Removido                 |                                |
+----------+-----------------+--------------------------+--------------------------------+
| Asus     | KFSN4-DRE       | Removido                 |                                |
+----------+-----------------+--------------------------+--------------------------------+
| Asus     | KGPE-D16        | Removido                 |                                |
+----------+-----------------+--------------------------+--------------------------------+
| Gigabyte | D945GCLF2D      | Similar a Intel D945GCLF |                                |
+----------+-----------------+--------------------------+--------------------------------+
| Gigabyte | GA-G41M-ES2L    | Sin mantenimiento        |                                |
+----------+-----------------+--------------------------+--------------------------------+
| Intel    | D410PT          | Sin mantenimiento        |                                |
+----------+-----------------+--------------------------+--------------------------------+
| Intel    | D510MO          | Sin mantenimiento        |                                |
+----------+-----------------+--------------------------+--------------------------------+
| Intel    | D945GCLF        | Sin mantenimiento        | nuevo controlador de GPU       |
+----------+-----------------+--------------------------+--------------------------------+
| Lenovo   | ThinkPad R400   | Mantenido                | nuevo controlador de GPU       |
+----------+-----------------+--------------------------+--------------------------------+
| Lenovo   | ThinkPad R500   | Mantenido                | nuevo controlador de GPU       |
+----------+-----------------+--------------------------+--------------------------------+
| Lenovo   | ThinkPad T400   | Mantenido                | nuevo controlador de GPU       |
+----------+-----------------+--------------------------+--------------------------------+
| Lenovo   | ThinkPad T400s  | Similar a ThinkPad T400  | nuevo controlador de GPU       |
+----------+-----------------+--------------------------+--------------------------------+
| Lenovo   | ThinkPad T500   | Mantenido                | nuevo controlador de GPU       |
+----------+-----------------+--------------------------+--------------------------------+
| Lenovo   | ThinkPad T60    | Mantenido                | controlador de GPU antiguo     |
|          | con GPU intel   |                          |                                |
+----------+-----------------+--------------------------+--------------------------------+
| Lenovo   | ThinkPad W500   | Mantenido                | nuevo controlador de GPU       |
+----------+-----------------+--------------------------+--------------------------------+
| Lenovo   | ThinkPad X200   | Mantenido                | nuevo controlador de GPU       |
+----------+-----------------+--------------------------+--------------------------------+
| Lenovo   | ThinkPad X200s  | Mantenido                | nuevo controlador de GPU       |
+----------+-----------------+--------------------------+--------------------------------+
| Lenovo   | ThinkPad X200T  | Mantenido                | nuevo controlador de GPU       |
+----------+-----------------+--------------------------+--------------------------------+
| Lenovo   | ThinkPad X301   | Mantenido                | nuevo controlador de GPU       |
+----------+-----------------+--------------------------+--------------------------------+
| Lenovo   | ThinkPad X60    | Mantenido                | controlador de GPU antiguo     |
+----------+-----------------+--------------------------+--------------------------------+
| Lenovo   | ThinkPad X60T   | Mantenido                | controlador de GPU antiguo     |
+----------+-----------------+--------------------------+--------------------------------+
| Lenovo   | ThinkPad X60s   | Mantenido                | controlador de GPU antiguo     |
+----------+-----------------+--------------------------+--------------------------------+

Estado de Coreboot:
-------------------

* Mantenido/Sin mantenimiento: GNU Boot depende de Coreboot para admitir
  computadoras específicas. Entonces, si Coreboot deja de admitir una
  computadora que admite GNU Boot, con el tiempo será cada vez más difícil
  admitir esa computadora en GNU Boot y, en algún momento, GNU Boot
  probablemente dejará de admitirla a menos que alguien vuelva a agregar
  esa computadora en Coreboot. Y si una computadora no tiene un mantenedor
  en Coreboot, aumenta la probabilidad de que sea eliminada en algún
  momento.

* Removido: Coreboot ya eliminó el soporte para esta computadora.

* Similar a <otra computadora>: No hay soporte oficial para esta
  computadora en Coreboot, pero usar el código de la otra computadora
  mencionada funciona.

Controlador de GPU Coreboot:
----------------------------

* Controlador de GPU antiguo/controlador de GPU nuevo: algunas
  computadoras utilizan un controlador de GPU antiguo escrito en C que
  podría tener algunos problemas de estabilidad que se solucionaron en el
  nuevo controlador de GPU escrito en ADA. Cuando el campo está en blanco,
  significa que no miramos qué controlador de GPU se usó ni su
  estabilidad.
