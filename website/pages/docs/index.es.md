---
title: Documentación
...

Manuales
========

El proyecto GNU Boot comenzó a crear un manual en
([línea](../manual/gnuboot.html), [pdf](../manual/gnuboot.pdf)) para
organizar mejor la documentación de GNU Boot. También permite
dirigirse a una mayor diversidad de usuarios, colaboradores y
mantenedores.

Tenga en cuenta que el manual en línea también puede guardarse
localmente. En ese caso, es conveniente guardar también la imagen
[SOIC-16.jpeg](../manual/images/SOIC-16.jpeg) en el mismo directorio
para que la imagen o imágenes funcionen.

Información general
===================

-   Puede encontrar información sobre dónde encontrar noticias de GNU Boot,
    incluyendo anuncios de versiones, en la [página de noticias](../news.md).

-   [Respuestas a las preguntas más frecuentes sobre GNU Boot](../faq.md).

Instalación y actualización de GNU Boot
=======================================

-   [¿En qué sistemas puedo usar GNU Boot?](hardware/)
-   [Cómo instalar GNU Boot](install/)
-   [¿Cómo sé qué versión estoy ejecutando?](install/version.md)

Documentación relacionada con los sistemas operativos
=====================================================

-   [Guías GNU+Linux](gnulinux/)
-   [Cómo instalar BSD en un sistema libreboot](bsd/)

Información para desarrolladores
================================

-   [Cómo compilar el código fuente de GNU Boot](build/)
-   [Historia y procedencia del código fuente de GNU Boot](history/)
-   [Depthcharge payload](depthcharge/) (**Sólo en Libreboot 20160907**)
-   [GRUB payload](grub/)

Información adicional
=====================

-   [Miscelánea](misc/)
-   [Lista de nombres clave](misc/codenames.md)
