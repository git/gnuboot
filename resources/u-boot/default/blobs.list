arch/x86/dts/microcode/

# The license is nonfree because it contains the following: "Reverse
# engineering, decompilation, or disassembly of this software is not
# permitted."
Licenses/r8a779x_usb3.txt
drivers/usb/host/xhci-rcar-r8a779x_usb3_v3.h

# The documentation contains instructions to download and install nonfree
# software. Note that if a board doesn't have such instructions it doesn't
# necessarily means that it can boot with only free software and viceversa.

###########
# Amlogic #
###########
# Amlogic SOCs Usually have various nonfree components, like the first stages
# of the bootloaders and code that runs in TrustZone. They are most likely
# not signed.
# ---------
# TODO: List the nonfree software of specific documentation
doc/board/amlogic/beelink-gtkingpro.rst
doc/board/amlogic/beelink-gtking.rst
doc/board/amlogic/index.rst
doc/board/amlogic/khadas-vim2.rst
doc/board/amlogic/khadas-vim3l.rst
doc/board/amlogic/khadas-vim3.rst
doc/board/amlogic/khadas-vim.rst
doc/board/amlogic/libretech-ac.rst
doc/board/amlogic/libretech-cc.rst
doc/board/amlogic/nanopi-k2.rst
doc/board/amlogic/odroid-c2.rst
doc/board/amlogic/odroid-c4.rst
doc/board/amlogic/odroid-n2.rst
doc/board/amlogic/p200.rst
doc/board/amlogic/p201.rst
doc/board/amlogic/p212.rst
doc/board/amlogic/q200.rst
doc/board/amlogic/s400.rst
doc/board/amlogic/sei510.rst
doc/board/amlogic/sei610.rst
doc/board/amlogic/u200.rst
doc/board/amlogic/w400.rst
doc/board/amlogic/wetek-core2.rst

#########
# Linux #
#########
# Has intructions to build Linux which is not FSDG compliant.
# TODO: Use linux-libre instead, especially because documentation about vboot
# could be interesting to have. Vboot is a chain of trust that can work with
# only free software. The hardware root of trust can be created by booting on
# a flash chip whose security registers are configured to set the first
# bootloader component read-only.
doc/uImage.FIT/beaglebone_vboot.txt
# Steers very strongly users into using Linux as it shows that the only tested
# kernels are Broadcom forks of Linux. We would need to have linux-libre
# versions of these or test it with stock linux-libre instead.
doc/README.bcm7xxx

############
# Mediatek #
############
# The instructions uses binaries that lack any corresponding source code.
doc/README.mediatek

#############
# NXP I.MX8 #
#############
# I.MX8 SOCs require a nonfree firmware for the DDR4 controller. In some
# documentation, I didn't find that requirement mentioned, but instead
# there are still nonfree files mentioned. So I assume that they might
# somehow contain code for that nonfree DDR4 controller, but it might be
# worth checking if it's the case or not. The DDR4 controller firmware is not
# signed. In addition the I.MX8 HDMI controller requires a signed firmware.
# -----------
# nonfree DDR4 controller firmware
doc/board/freescale/imx8mp_evk.rst
# nonfree DDR4 controller and HDMI firmwares
doc/board/freescale/imx8mq_evk.rst
# nonfree DDR4 controller firmware
doc/board/freescale/imx8mn_evk.rst
# nonfree imx-sc-firmware-1.2.7.1.bin and imx-seco-2.3.1.bin firmwares
doc/board/freescale/imx8qxp_mek.rst
# nonfree DDR4 controller firmware
doc/board/freescale/imx8mm_evk.rst
# nonfree imx-sc-firmware-1.1.bin and firmware-imx-8.0.bin firmwares
doc/board/advantech/imx8qm-rom7720-a1.rst
# TODO
doc/board/verdin-imx8mm.rst
doc/board/toradex/colibri-imx8x.rst
doc/board/toradex/apalix-imx8x.rst
doc/board/toradex/apalix-imx8.rst

#######################
# NXP nonfree srktool #
#######################
# The SRK tool is a tool that is involved in one way or another with
# authenticated or encrypted boot. I'm unsure if free software replacements
# exists or if could easily be replaced with a free software implementation.
# In any case the I.MX6 and I.MX5 can proabably be setup for encrypted or
# authenticated boot with free software tools. The first and second versions
# of the USB Armory has documentation on how to do that.
# ---------------------
doc/imx/board/toradex/colibri_imx7.rst
doc/imx/habv4/introduction_habv4.txt

##################
# Samsung Exynos #
##################
# The instructions makes users nonfree components like a nonfree first stage
# bootloaders, and nonfree code that runs in TrustZone.
doc/README.odroid
# The instructions makes its users download an image and update u-boot in that
# image. Because of that, it's extremely likely that the images contains
# nonfree components that cannot even be redistributed in another form, and
# that the instructions uses that images because of that.
doc/README.s5p4418

#####################
# Texas Instruments #
#####################
# Users are expected to use nonfree tools and even sign an NDA to get access
# to them.
doc/README.ti-secure

###########
# Unknown #
###########
# Everything looks free software, but the code still needs to be reviewed.
doc/board/microchip/mpfs_icicle.rst
# OP-TEE is under a free software license but its code needs to be reviewed.
doc/README.tee
# The tutorial has instructions to download a downstream u-boot, so it might
# have the same issues than u-boot itself if the u-boot is recent enough.
doc/chromium/run_vboot.rst

#######
# x86 #
#######
# Unless the computer is supported by Libreboot, or that u-boot runs after
# some other nonfree boot software like a BIOS or UEFI, it's unlikely to be
# able to run with only free software. Though I'm pretty sure that some
# exceptions do exists, but they are probably not supported by u-boot.
# -----
# nonfree Management Engine firmware, RAM intialization code, and video BIOS
doc/board/google/chromebook_link.rst
# nonfree SDRAM and hardware intialization code
doc/board/google/chromebook_coral.rst

# nonfree FSP, video BIOS, Management Engine firmware
doc/board/intel/minnowmax.rst
# nonfree FSP, Chipset Micro Code (CMC), microcode
doc/board/intel/crownbay.rst

# TODO: check
# board/intel/edison.rst
# Steers userstoward using nonfree FSP
board/intel/slimbootloader.rst

# Steers users and developers toward using nonfree FSP
doc/device-tree-bindings/fsp/fsp2/apollolake/fsp-m.txt

# Steers users and developers toward using nonfree FSP
doc/device-tree-bindings/fsp/fsp2/apollolake/fsp-s.txt

############
# Rockchip #
############
# rkbin binaries without license nor source code
doc/board/rockchip/rockchip.rst

# TODO: check the following files
# imx/common/mxs.txt
# README.armada-secureboot
# README.fdt-control
# README.fsl-ddr
# README.m54418twr
# README.marvell
# README.mpc85xxcds
# README.mpc85xx-sd-spi-boot
# README.OFT
# README.rmobile
# README.rockchip
# README.rockusb
# README.socfpga
