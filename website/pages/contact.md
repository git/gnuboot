---
title: Contact
...


Mailing lists
=============

GNU Boot makes announcements in [the gnuboot-announce mailing
list](//lists.gnu.org/mailman/listinfo/gnuboot-announce).

The [gnuboot mailing list](//lists.gnu.org/mailman/listinfo/gnuboot)
is the main channel for discussion.

The [bug-gnuboot mailing
list](//lists.gnu.org/mailman/listinfo/bug-gnuboot) is for user
support and bug reporting

And the [gnuboot-patches mailing
list](//lists.gnu.org/mailman/listinfo/gnuboot-patches) is where to
send patches.

IRC chatroom
============

We have an IRC chatroom on Liberat.chat :
[#gnuboot](https://web.libera.chat/#gnuboot)

Social media
============

Currently GNU Boot does not exist officially on social media.
