---
title: ¡Libera tu BIOS hoy mismo!
...

### ¿Qué es esto? ###

GNU Boot es un proyecto de software 100% libre destinado a sustituir el
software de arranque no libre (como BIOS o UEFI) de los computadores por
software de arranque libre.

GNU Boot es sólo una distribución: reutiliza proyectos de software
existentes como Coreboot, GRUB, SeaBIOS, etc.

Así que no es muy diferente de [distribuciones GNU/Linux 100%
libres](https://www.gnu.org/distros/free-distros.es.html) como Trisquel o
Guix.

Como el resto de distribuciones GNU/Linux 100% libres sus tareas son:

* Encontrar y eliminar el software no libre de los proyectos que
  reutiliza.

* Construir el software y ensamblarlo en algo que pueda instalarse.
  Probarlo y proporcionar instrucciones de instalación y actualización.

* Proporcionar una buena configuración por defecto que funcione para la
  mayoría de los usuarios.

### Cómo surgió este proyecto ###

Creemos que los usuarios de computadores merecen controlar todo el
software que ejecutan. Esta creencia es el principio clave del
movimiento del software libre, y fue el motivo para desarrollar el
sistema operativo GNU y crear la Free Software Foundation. Creemos que
la libertad de los usuarios de computadores es un derecho humano
crucial.

Para lograr nuestro objetivo, es crucial que llamemos «libre» a un
programa sólo cuando sea realmente libre en su totalidad. Cuando
hablemos de programas concretos, no debemos confundir los hechos sobre
lo que es libre y lo que no. Si la gente empieza a referirse a un
programa como «libre», cuando partes de él no lo son en realidad, eso
tiende a llevar a la comunidad por mal camino.

Por desgracia, el año pasado se produjo un embrollo de este tipo con un
programa de arranque que era software libre y que se denominó Libreboot:
el equipo de desarrollo le añadió código que no era libre, pero siguió
utilizando «Libre» en su nombre.

Libreboot se lanzó por primera vez en 2013. Ha sido ampliamente
recomendado en la comunidad del software libre durante los últimos nueve
años. En noviembre de 2022, Libreboot comenzó a incluir código no libre.
Hemos hecho repetidos esfuerzos por seguir colaborando con dichos
desarrolladores para ayudar a que su versión de Libreboot siga siendo
libre, pero no hemos tenido éxito.

Ahora hemos dado un paso adelante para defender la libertad, la nuestra
y la de la comunidad en general, manteniendo nuestra propia versión, un
Libreboot genuinamente libre, que tras algunos obstáculos dio origen a
este proyecto: GNU Boot.
