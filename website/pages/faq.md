---
title: Frequently Asked Questions
x-unreviewed: true
...

AKA Frequently Questioned Answers

Important issues
================

How to compile GNU Boot from source
------------------------------------

Refer to the [lbmk build instructions](docs/build/).

Do not use CH341A!
------------------

This SPI flasher will damage your chip, and the mainboard that it is connected
to.

Read the notes about CH341A on [docs/install/spi.md](docs/install/spi.md) to
learn more.

Flashrom complains about DEVMEM access
--------------------------------------

If running `flashrom -p internal` for software based flashing, and
you get an error related to /dev/mem access, you should reboot with
`iomem=relaxed` kernel parameter before running flashrom, or use a kernel
that has `CONFIG_STRICT_DEVMEM` and `CONFIG_IO_STRICT_DEVMEM` not enabled.

Example flashrom output with both `CONFIG_STRICT_DEVMEM` and `CONFIG_IO_STRICT_DEVMEM` enabled:
```
flashrom v0.9.9-r1955 on Linux 4.11.9-1-ARCH (x86_64)
flashrom is free software, get the source code at https://flashrom.org

Calibrating delay loop... OK.
Error accessing high tables, 0x100000 bytes at 0x000000007fb5d000
/dev/mem mmap failed: Operation not permitted
Failed getting access to coreboot high tables.
Error accessing DMI Table, 0x1000 bytes at 0x000000007fb27000
/dev/mem mmap failed: Operation not permitted
```

The backlight is darker on the left side of the screen when lowering the brightness on my ThinkPad X200/X200S/X200T, T400, T500, R400, W500, R500 and other Intel laptops
---------------------------------------------------------------------------------------------------------------

We don't know how to detect the correct PWM value to use in
coreboot-libre, so we just use the default one in coreboot which has
this issue on some CCFL panels, but not LED panels.

You can work around this in your distribution, by following the notes at
[docs: backlight control](../docs/misc/#finetune-backlight-control-on-intel-gpus).

The ethernet doesn't work on my X200/T400/X60/T60 when I plug in it 
-------------------------------------------------------------------

This was observed on some systems using network-manager. This happens
both on the original BIOS and in GNU Boot. It's a quirk in the
hardware. On debian systems, a workaround is to restart the networking
service when you connect the ethernet cable:

    sudo service network-manager restart

On Parabola, you can try:

    sudo systemctl restart network-manager

(the service name might be different for you, depending on your
configuration)

My KCMA-D8 or KGPE-D16 doesn't boot with the PIKE2008 module installed 
-----------------------------------------------------------------------

Libreboot 20160818, 20160902 and 20160907 all have this behaviour: in SeaBIOS,
PCI options ROMs are loaded when available, by default. This is not
technically a problem, because an option ROM can be free or non-free.

Loading the option ROM from the PIKE2008 module on either ASUS KCMA-D8
or KGPE-D16 causes the system to hang at boot. It's possible to use
this in the payload (if you use a Linux kernel payload, like LinuxBoot),
or to boot (with SeaGRUB and/or SeaBIOS) from regular SATA and then use
it in GNU+Linux. The Linux kernel is capable of using the PIKE2008
module without loading the option ROM.

Refer to the [LinuxBoot website](https://www.linuxboot.org/). This is a special
system that uses BusyBox and the Linux kernel, which goes in the boot flash as a
coreboot payload. You can insert it into your GNU Boot image using cbfstool, if
it's big enough. On KCMA-D8/KGPE-D16 it's trivial to upgrade the boot flash to
16MiB, which is more than enough to fit LinuxBoot. See:\
[External flashing guide](docs/install/spi.md)

LinuxBoot has many advanced features. It provides a bootloader called uroot,
which can boot other Linux kernels using kexec. It can parse GNU GRUB configs,
and it can also provide some basic UEFI features. Because it's using the Linux
kernel, this means that LinuxBoot can make use of the PIKE2008 module.

TODO: Integrate this in GNU Boot, as a payload option, but also:

TODO: Fork LinuxBoot, and make a version of it that uses Linux-libre. Ensure
that it is a fully free distribution, so that the FSF can endorse it.

LinuxBoot is *perfect*, especially if you're setting up one of these systems to
be used as a server. LinuxBoot is *much* more flexible and configurable than
GNU GRUB, which right now is the payload that GNU Boot prefers.

How to save kernel panic logs on thinkpad laptops?
--------------------------------------------------

The easiest method of doing so is by using the kernel's netconsole
and reproducing the panic. Netconsole requires two machines, the one that is
panicky (source) and the one that will receive crash logs (target). The
source has to be connected with an ethernet cable and the target has to be
reachable at the time of the panic. To set this system up, execute the
following commands as root on the source (`source#`) and normal user on
the target (`target$`):

1.  Start a listener server on the target machine (netcat works well):

    `target$ nc -u -l -p 6666`

2.  Mount configfs (only once per boot, you can check if it is already mounted
    with `mount | grep /sys/kernel/config`. This will return no output
    if it is not).

    `source# modprobe configfs`

    `source# mkdir -p /sys/kernel/config`

    `source# mount none -t configfs /sys/kernel/config`

3.  find source's ethernet interface name, it should be of the form `enp*` or
    `eth*`, see `ip address` or `ifconfig` output.

    `source# iface="enp0s29f8u1"` change this

    Fill the target machine's IPv4 address here:

    `source# tgtip="192.168.1.2"` change this


3.  Create netconsole logging target on the source machine:

    `source# modprobe netconsole`

    `source# cd /sys/kernel/config/netconsole`

    `source# mkdir target1; cd target1`

    `source# srcip=$(ip -4 addr show dev "$iface" | grep -Eo '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+')`

    `source# echo "$srcip" > local_ip`

    `source# echo "$tgtip" > remote_ip`

    `source# echo "$iface" > dev_name`

    `source# arping -I "$iface" "$tgtip" -f | grep -o '..:..:..:..:..:..' > remote_mac`

    `source# echo 1 > enabled`

4.  Change console loglevel to debugging:

    `source# dmesg -n debug`

5.  Test if the logging works by e.g. inserting or removing an USB
    device on the source. There should be a few lines appearing in the
    terminal, in which you started netcat (nc), on the target host.

6.  Try to reproduce the kernel panic.

Machine check exceptions on some Montevina (Penryn CPU) laptops
---------------------------------------------------------------

Some GM45 laptops have been freezing or experiencing a kernel panic
(blinking caps lock LED and totaly unresponsive machine, sometimes followed
by an automatic reboot within 30 seconds).
We do not know what the problem(s) is(are), but a CPU microcode
update in some cases prevents this from happening again.
See the following bug reports for more info:

- [T400 Machine check: Processor context corrupt](http://web.archive.org/web/20210325195107/https://notabug.org/libreboot/libreboot/issues/493)
- [X200 Machine check: Processor context corrupt](http://web.archive.org/web/20210128161939/https://notabug.org/libreboot/libreboot/issues/289)

- [Unrelated, RAM incompatibility and suspend-to-ram issues on X200](https://libreboot.srht.site/docs/hardware/x200.html#ram_s3_microcode)


Hardware compatibility
======================

What systems are compatible with GNU Boot?
-----------------------------------------------------------------------------------

See the [hardware compatibility list](docs/hardware/).

Can you add support for more computers?
---------------------------------------

Unless GNU Boot already works on computers that for some reasons are
not yet listed as supported, there will be some more work to do than
just testing and reporting what works.

GNU Boot is a 100% free distribution similar to other 100% free
distributions like Parabola or Trisquel, and like Parabola or
Trisquel, it reuses other software to make something that can be
installed.

Most of the work on GNU Boot consists in testing already supported
computers, improving the documentation and various packaging work.

Like with many other free software projects, the GNU Boot maintainers
are very busy running the project and doing improvements that will
benefit the project in the long term, they most likely don't have the
time to add support for newer computers themselves at the moment, but
they can help you getting the job done with some guidance and by
reviewing patches.

So if you want to add a new computer, the first part of the job is to
verify if the computer can boot and is usable without nonfree
software. It's a good idea to start by reading the "Hardware
compatibility" section of this FAQ to avoid the most common mistakes
with that.

Then once you're confident enough that your computer can boot with
fully free software, you can open a bug report and/or contact the GNU
boot project in one of its mailing list to notify the GNU Boot
maintainers and other contributors about that as this way the
information about this computer will not be lost.

This way if you don't have time anymore to work on it, maybe it would
interest other people later on, or maybe not. In addition, this will
help you getting some feedback from other people to help you
understand if you're on the the right track or not with the computer
you want to add support for.

Then once this is done, the computer will need to be supported well by
other project: like Parabola or Trisquel, GNU Boot reuses other
projects to support hardware. For instance Parabola relies on
linux-libre for its drivers.

GNU Boot relies on software like Coreboot or U-Boot instead. So you
will have to add support for your computer in such projects. This
can be very complicated to do if you're not used to work on low-level
software like drivers, kernels, microcontrollers, etc, unless there is
already a computer very similar to the one you want to add support for
(in that case it might be a good way to get started, though expect to
have to learn many things along the way).

Once this is done, you can add support for that computer in GNU Boot
by doing some packaging work, testing, and writing some
documentation. This is relatively easy to do.

The documentation on how to send patches to GNU Boot is available in
the [contribute.md](contribute.md) page.

Will the Purism laptops be supported?
----------------------------------------------------------------------

Short answer: no.

There are severe privacy, security and freedom issues with these laptops, due
to the Intel chipsets that they use. See:

- [Intel Management Engine](#intelme)
- [More freedom issues on modern Intel hardware](#intel)

Most notably, these laptops also use the Intel FSP binary blob, for the entire
hardware initialization. Coreboot does support a particular revision of one of
their laptops, but most are either unsupported or rely on binary blobs for most
of the hardware initialization.

In particular, the Intel Management Engine is a severe threat to privacy and
security, not to mention freedom, since it is a remote backdoor that provides
Intel remote access to a computer where it is present.

Intel themselves even admitted it, publicly.

Why is the latest Intel hardware unsupported in GNU Boot? {#intel}
-----------------------------------------------------------

It is unlikely that any post-2008 Intel hardware will ever be supported in
GNU Boot, due to severe security and freedom issues; so severe, that *the
GNU Boot project recommends avoiding all modern Intel hardware. If you have an
Intel based system affected by the problems described below, then you should
get rid of it as soon as possible*. The main issues are as follows:

### Intel Management Engine (ME) {#intelme}

NOTE: The information below is slightly out of date. Nowadays, Intel ME does
not run on an ARC coprocessor, but instead runs on a modified Intel 486 based
architecture, with the ME firware written for x86 based on the Minix operating
system. However, the overall design philosophy and operation is mostly the
same.

On *most* current Intel platforms that have Intel ME, it is possible to
partly disable it. See:

<https://github.com/corna/me_cleaner>\
NOTE: on those systems, the ME firmware is still needed in the boot flash, and
since it is a binary blob, those systems are not supported in GNU Boot.

On all GNU Boot systems that have an Intel ME in it (GM45+ICH9M laptops and
X4X+ICH10 desktops), the ME firmware is not needed in the boot flash. Either a
modified descriptor is used, which disables the ME and removes the region for
it in the boot flash, or a descriptorless setup is used.

Now onto the main topic:

Introduced in June 2006 in Intel's 965 Express Chipset Family of
(Graphics and) Memory Controller Hubs, or (G)MCHs, and the ICH8 I/O
Controller Family, the Intel Management Engine (ME) is a separate
computing environment physically located in the (G)MCH chip. In Q3 2009,
the first generation of Intel Core i3/i5/i7 (Nehalem) CPUs and the 5
Series Chipset family of Platform Controller Hubs, or PCHs, brought a
more tightly integrated ME (now at version 6.0) inside the PCH chip,
which itself replaced the ICH. Thus, the ME is ***present on all Intel
desktop, mobile (laptop), and server systems since mid 2006***.

The ME consists of an ARC processor core (replaced with other processor
cores in later generations of the ME), code and data caches, a timer,
and a secure internal bus to which additional devices are connected,
including a cryptography engine, internal ROM and RAM, memory
controllers, and a ***direct memory access (DMA) engine*** to access the
host operating system's memory as well as to reserve a region of
protected external memory to supplement the ME's limited internal RAM.
The ME also has ***network access*** with its own MAC address through an
Intel Gigabit Ethernet Controller. Its boot program, stored on the
internal ROM, loads a firmware "manifest" from the PC's SPI flash
chip. This manifest is ***signed with a strong cryptographic key***,
which differs between versions of the ME firmware. If the manifest
isn't signed by a specific Intel key, the boot ROM won't load and
execute the firmware and the ME processor core will be halted.

The ME firmware is compressed and consists of modules that are listed in
the manifest along with secure cryptographic hashes of their contents.
One module is the operating system kernel, which is based on a
***proprietary real-time operating system (RTOS) kernel*** called
"ThreadX". The developer, Express Logic, sells licenses and source
code for ThreadX. Customers such as Intel are forbidden from disclosing
or sublicensing the ThreadX source code. Another module is the Dynamic
Application Loader (DAL), which consists of a ***Java virtual machine***
and set of preinstalled Java classes for cryptography, secure storage,
etc. The DAL module can load and execute additional ME modules from the
PC's HDD or SSD. The ME firmware also includes a number of native
application modules within its flash memory space, including Intel
Active Management Technology (AMT), an implementation of a Trusted
Platform Module (TPM), Intel Boot Guard, and audio and video DRM
systems.

The Active Management Technology (AMT) application, part of the Intel
"vPro" brand, is a Web server and application code that enables remote
users to power on, power off, view information about, and otherwise
manage the PC. It can be ***used remotely even while the PC is powered
off*** (via Wake-on-Lan). Traffic is encrypted using SSL/TLS libraries,
but recall that all of the major SSL/TLS implementations have had highly
publicized vulnerabilities. The AMT application itself has ***[known
vulnerabilities](https://en.wikipedia.org/wiki/Intel_Active_Management_Technology#Known_vulnerabilities_and_exploits)***,
which have been exploited to develop rootkits and keyloggers and
covertly gain encrypted access to the management features of a PC.
Remember that the ME has full access to the PC's RAM. This means that
an attacker exploiting any of these vulnerabilities may gain access to
everything on the PC as it runs: all open files, all running
applications, all keys pressed, and more.

[Intel Boot Guard](https://mjg59.dreamwidth.org/33981.md) is an ME
application introduced in Q2 2013 with ME firmware version 9.0 on 4th
Generation Intel Core i3/i5/i7 (Haswell) CPUs. It allows a PC OEM to
generate an asymmetric cryptographic keypair, install the public key in
the CPU, and prevent the CPU from executing boot firmware that isn't
signed with their private key. This means that ***coreboot and GNU Boot
are impossible to port*** to such PCs, without the OEM's private
signing key. Note that systems assembled from separately purchased
mainboard and CPU parts are unaffected, since the vendor of the
mainboard (on which the boot firmware is stored) can't possibly affect
the public key stored on the CPU.

ME firmware versions 4.0 and later (Intel 4 Series and later chipsets)
include an ME application for ***audio and video
[DRM](https://defectivebydesign.org/what_is_drm_digital_restrictions_management)***
called "Protected Audio Video Path" (PAVP). The ME receives from the
host operating system an encrypted media stream and encrypted key,
decrypts the key, and sends the encrypted media decrypted key to the
GPU, which then decrypts the media. PAVP is also used by another ME
application to draw an authentication PIN pad directly onto the screen.
In this usage, the PAVP application directly controls the graphics that
appear on the PC's screen in a way that the host OS cannot detect. ME
firmware version 7.0 on PCHs with 2nd Generation Intel Core i3/i5/i7
(Sandy Bridge) CPUs replaces PAVP with a similar DRM application called
"Intel Insider". Like the AMT application, these DRM applications,
which in themselves are defective by design, demonstrate the omnipotent
capabilities of the ME: this hardware and its proprietary firmware can
access and control everything that is in RAM and even ***everything that
is shown on the screen***.

The Intel Management Engine with its proprietary firmware has complete
access to and control over the PC: it can power on or shut down the PC,
read all open files, examine all running applications, track all keys
pressed and mouse movements, and even capture or display images on the
screen. And it has a network interface that is demonstrably insecure,
which can allow an attacker on the network to inject rootkits that
completely compromise the PC and can report to the attacker all
activities performed on the PC. It is a threat to freedom, security, and
privacy that can't be ignored.

Before version 6.0 (that is, on systems from 2008/2009 and earlier), the
ME can be disabled by setting a couple of values in the SPI flash
memory. The ME firmware can then be removed entirely from the flash
memory space. GNU Boot [does this](docs/install/ich9utils.md) on
the Intel 4 Series systems that it supports, such as the [GNU Boot
X200](../docs/install/x200_external.md) and [GNU Boot
T400](../docs/install/t400_external.md). ME firmware versions 6.0 and
later, which are found on all systems with an Intel Core i3/i5/i7 CPU
and a PCH, include "ME Ignition" firmware that performs some hardware
initialization and power management. If the ME's boot ROM does not find
in the SPI flash memory an ME firmware manifest with a valid Intel
signature, the whole PC will shut down after 30 minutes.

Due to the signature verification, developing free replacement firmware
for the ME is basically impossible. The only entity capable of replacing
the ME firmware is Intel. As previously stated, the ME firmware includes
proprietary code licensed from third parties, so Intel couldn't release
the source code even if they wanted to. And even if they developed
completely new ME firmware without third-party proprietary code and
released its source code, the ME's boot ROM would reject any modified
firmware that isn't signed by Intel. Thus, the ME firmware is both
hopelessly proprietary and "tivoized".

**In summary, the Intel Management Engine and its applications are a
backdoor with total access to and control over the rest of the PC. The
ME is a threat to freedom, security, and privacy, and the GNU Boot
project strongly recommends avoiding it entirely. Since recent versions
of it can't be removed, this means avoiding all recent generations of
Intel hardware.**

More information about the Management Engine can be found on various Web
sites, including [me.bios.io](http://me.bios.io/Main_Page),
[unhuffme](http://io.netgarage.org/me/), [coreboot
wiki](http://www.coreboot.org/Intel_Management_Engine), and
[Wikipedia](https://en.wikipedia.org/wiki/Intel_Active_Management_Technology).
The book ***[Platform Embedded Security Technology
Revealed](https://www.apress.com/9781430265719)*** describes in great
detail the ME's hardware architecture and firmware application modules.

If you're stuck with the ME (non-GNU Boot system), you might find this
interesting:
<http://hardenedlinux.org/firmware/2016/11/17/neutralize_ME_firmware_on_sandybridge_and_ivybridge.html>

Also see (effort to disable the ME):
<https://www.coreboot.org/pipermail/coreboot/2016-November/082331.html>
- look at the whole thread

### Firmware Support Package (FSP) {#fsp}

On all recent Intel systems, coreboot support has revolved around
integrating a blob (for each system) called the *FSP* (firmware support
package), which handles all of the hardware initialization, including
memory and CPU initialization. Reverse engineering and replacing this
blob is almost impossible, due to how complex it is. Even for the most
skilled developer, it would take years to replace. Intel distributes
this blob to firmware developers, without source.

Since the FSP is responsible for the early hardware initialization, that
means it also handles SMM (System Management Mode). This is a special
mode that operates below the operating system level. **It's possible
that rootkits could be implemented there, which could perform a number
of attacks on the user (the list is endless). Any Intel system that has
the proprietary FSP blob cannot be trusted at all.** In fact, several
SMM rootkits have been demonstrated in the wild (use a search engine to
find them).

### CPU microcode updates {#microcode}

All modern x86 CPUs (from Intel and AMD) use what is called *microcode*.
CPUs are extremely complex, and difficult to get right, so the circuitry
is designed in a very generic way, where only basic instructions are
handled in hardware. Most of the instruction set is implemented using
microcode, which is low-level software running inside the CPU that can
specify how the circuitry is to be used, for each instruction. The
built-in microcode is part of the hardware, and read-only. Both the
circuitry and the microcode can have bugs, which could cause reliability
issues.

Microcode *updates* are proprietary blobs, uploaded to the CPU at boot
time, which patches the built-in microcode and disables buggy parts of
the CPU to improve reliability. In the past, these updates were handled
by the operating system kernel, but on all recent systems it is the boot
firmware that must perform this task. Coreboot does distribute microcode
updates for Intel and AMD CPUs, but GNU Boot cannot, because the whole
point of GNU Boot is to be 100% [free
software](https://www.gnu.org/philosophy/free-sw.html).

On some older Intel CPUs, it is possible to exclude the microcode
updates and not have any reliability issues in practice. All current
GNU Boot systems work without microcode updates (otherwise, they
wouldn't be supported in GNU Boot). However, all modern Intel CPUs
require the microcode updates, otherwise the system will not boot at
all, or it will be extremely unstable (memory corruption, for example).

Intel CPU microcode updates are *signed*, which means that you could not
even run a modified version, even if you had the source code. If you try
to upload your own modified updates, the CPU will reject them.

The microcode updates alter the way instructions behave on the CPU. That
means they affect the way the CPU works, in a very fundamental way. That
makes it software. The updates are proprietary, and are software, so we
exclude them from GNU Boot. The microcode built into the CPU already is
not so much of an issue, since we can't change it anyway (it's
read-only).

### Intel is uncooperative 

For years, coreboot has been struggling against Intel. Intel has been
shown to be extremely uncooperative in general. Many coreboot
developers, and companies, have tried to get Intel to cooperate; namely,
releasing source code for the firmware components. Even Google, which
sells millions of *chromebooks* (coreboot preinstalled) have been unable
to persuade them.

Even when Intel does cooperate, they still don't provide source code.
They might provide limited information (datasheets) under strict
corporate NDA (non-disclosure agreement), but even that is not
guaranteed. Even ODMs and IBVs can't get source code from Intel, in
most cases (they will just integrate the blobs that Intel provides).

Recent Intel graphics chipsets also [require firmware
blobs](https://01.org/linuxgraphics/intel-linux-graphics-firmwares?langredirect=1).

Intel is [only going to get
worse](https://www.phoronix.com/scan.php?page=news_item&px=Intel-Gfx-GuC-SLPC)
when it comes to user freedom. GNU Boot has no support for recent Intel
platforms, precisely because of the problems described above. The only
way to solve this is to get Intel to change their policies and to be
more friendly to the [free
software](https://www.gnu.org/philosophy/free-sw.html) community.
Reverse engineering won't solve anything long-term, unfortunately, but
we need to keep doing it anyway. Moving forward, Intel hardware is a
non-option unless a radical change happens within Intel.

**Basically, all Intel hardware from year 2010 and beyond will never be
supported by GNU Boot. The GNU Boot project is actively ignoring all
modern Intel hardware at this point, and focusing on alternative
platforms.**

Why is the latest AMD hardware unsupported in GNU Boot? {#amd}
----------------------------------------------------------------------------

It is extremely unlikely that any modern AMD hardware will ever be
supported in GNU Boot, due to severe security and freedom issues; so
severe, that *the GNU Boot project recommends avoiding all modern AMD
hardware. If you have an AMD based system affected by the problems
described below, then you should get rid of it as soon as possible*. The
main issues are as follows:

[We call on AMD to release source code and specs for the new AMD Ryzen
platforms! We call on the community to put pressure on AMD. Click here
to read more](amd-libre.md)

### AMD Platform Security Processor (PSP) 

This is basically AMD's own version of the [Intel Management
Engine](#intelme). It has all of the same basic security and freedom
issues, although the implementation is wildly different.

The Platform Security Processor (PSP) is built in on the AMD CPUs whose
[architecture](https://en.wikipedia.org/wiki/List_of_AMD_CPU_microarchitectures) is Late Family 16h (Puma), Zen 17h or later (and also on
the AMD GPUs which are GCN 5th gen (Vega) or later). On the CPUs, a PSP
controls the main x86 core startup. PSP firmware is cryptographically
signed with a strong key similar to the Intel ME. If the PSP firmware
is not present, or if the AMD signing key is not present, the x86 cores
will not be released from reset, rendering the system inoperable.

The PSP is an ARM core with TrustZone technology, built onto the main
CPU die. As such, it has the ability to hide its own program code,
scratch RAM, and any data it may have taken and stored from the
lesser-privileged x86 system RAM (kernel encryption keys, login data,
browsing history, keystrokes, who knows!). To make matters worse, the
PSP theoretically has access to the entire system memory space (AMD
either will not or cannot deny this, and it would seem to be required to
allow the DRM "features" to work as intended), which means that it has
at minimum MMIO-based access to the network controllers and any other
PCI/PCIe peripherals installed on the system.

In theory any malicious entity with access to the AMD signing key would
be able to install persistent malware that could not be eradicated
without an external flasher and a known good PSP image. Furthermore,
multiple security vulnerabilities have been demonstrated in AMD firmware
in the past, and there is every reason to assume one or more zero day
vulnerabilities are lurking in the PSP firmware. Given the extreme
privilege level (ring -2 or ring -3) of the PSP, said vulnerabilities
would have the ability to remotely monitor and control any PSP enabled
machine completely outside of the user's knowledge.

Much like with the Intel Boot Guard (an application of the Intel
Management Engine), AMD's PSP can also act as a tyrant by checking
signatures on any boot firmware that you flash, making replacement boot
firmware (e.g. GNU Boot, coreboot) impossible on some boards. Early
anecdotal reports indicate that AMD's boot guard counterpart will be
used on most OEM hardware, disabled only on so-called "enthusiast"
CPUs.

### AMD IMC firmware 

Read <https://www.coreboot.org/AMD_IMC>.

### AMD SMU firmware 

Handles some power management for PCIe devices (without this, your
laptop will not work properly) and several other power management
related features.

The firmware is signed, although on older AMD hardware it is a symmetric
key, which means that with access to the key (if leaked) you could sign
your own modified version and run it. Rudolf Marek (coreboot hacker)
found out how to extract this key [in this video
demonstration](https://media.ccc.de/v/31c3_-_6103_-_en_-_saal_2_-_201412272145_-_amd_x86_smu_firmware_analysis_-_rudolf_marek),
and based on this work, Damien Zammit (another coreboot hacker)
[partially replaced it](https://github.com/zamaudio/smutool/) with free
firmware, but on the relevant system (ASUS F2A85-M) there were still
other blobs present (Video BIOS, and others) preventing the hardware
from being supported in GNU Boot.

### AMD AGESA firmware 

This is responsible for virtually all core hardware initialization on
modern AMD systems. In 2011, AMD started cooperating with the coreboot
project, releasing this as source code under a free license. In 2014,
they stopped releasing source code and started releasing AGESA as binary
blobs instead. This makes AGESA now equivalent to [Intel FSP](#fsp).

### AMD CPU microcode updates 

Read the Intel section 
practically the same, though it was found with much later hardware in
AMD that you could run without microcode updates. It's unknown whether
the updates are needed on all AMD boards (depends on CPU).

### AMD is incompetent (and uncooperative) 

AMD seemed like it was on the right track in 2011 when it started
cooperating with and releasing source code for several critical
components to the coreboot project. It was not to be. For so-called
economic reasons, they decided that it was not worth the time to invest
in the coreboot project anymore. Unfortunately they haven't even shared
the source code of AGESA library for a Family 15h Steamroller/Excavator
architectures (which, like the earlier fam15h Bulldozer/Piledriver, do
not have a PSP) and released it to a coreboot project only as a binary.

For a company to go from being so good, to so bad, in just 3 years,
shows that something is seriously wrong with AMD. Like Intel, they do
not deserve your money.

Given the current state of Intel hardware with the Management Engine, it
is our opinion that all performant x86 hardware newer than the AMD
Family 15h CPUs (on AMD's side) or anything post-2009 on Intel's side
is defective by design and cannot safely be used to store, transmit, or
process sensitive data. Sensitive data is any data in which a data
breach would cause significant economic harm to the entity which created
or was responsible for storing said data, so this would include banks,
credit card companies, or retailers (customer account records), in
addition to the "usual" engineering and software development firms.
This also affects whistleblowers, or anyone who needs actual privacy and
security.

What *can* I use, then? {#whatcaniuse}
-------------------------

GNU Boot has support for AMD hardware of Family 15h (Bulldozer or
Piledriver, ~2012 gen) and some older Intel platforms like Napa,
Montevina, Eagle Lake, Lakeport (2004-2006). We also have support for
some ARM chipsets (rk3288). On the Intel side, we're also interested in
some of the chipsets that use Atom CPUs (rebranded from older chipsets,
mostly using ich7-based southbridges).

Will GNU Boot work on a ThinkPad T400 or T500 with an ATI GPU?
---------------------------------------------------------------------------------------------------

Short answer: yes. These laptops also have an Intel GPU inside, which
GNU Boot uses. The ATI GPU is ignored.

These laptops use what is called *switchable graphics*, where it will
have both an Intel and ATI GPU. Coreboot will allow you to set (using
nvramtool) a parameter, specifying whether you would like to use Intel
or ATI. The ATI GPU lacks free native graphics initialization in
coreboot, unlike the Intel GPU.

GNU Boot modifies coreboot, in such a way where this nvramtool setting
is ignored. GNU Boot will just assume that you want to use the Intel
GPU. Therefore, the ATI GPU is completely disabled on these laptops.
Intel is used instead, with the free native graphics initialization
(VBIOS replacement) that exists in coreboot.

Will desktop/server hardware be supported?
------------------------------------------------------------------------

GNU Boot now supports desktop hardware:
[(see list)](../docs/hardware/)
(with full native video initialization).

A common issue with desktop hardware is the Video BIOS, when no onboard
video is present, since every video card has a different Video BIOS.
Onboard GPUs also require one, so those still have to be replaced with
free software (non-trivial task). GNU Boot has to initialize the
graphics chipset, but most graphics cards lack a free Video BIOS for
this purpose. Some desktop motherboards supported in coreboot do have
onboard graphics chipsets, but these also require a proprietary Video
BIOS, in most cases.

Hi, I have &lt;insert random system here&gt;, is it supported?
--------------------------------------------------------------------------------------------------------

Most likely not. First, you must consult coreboot's own hardware
compatibility list at <http://www.coreboot.org/Supported_Motherboards>
and, if it is supported, check whether it can run without any
proprietary blobs in the image. If it can: wonderful! GNU Boot can
support it, and you can add support for it. If not, then you will need
to figure out how to reverse engineer and replace (or remove) those
blobs that do still exist, in such a way where the system is still
usable in some defined way.

For those systems where no coreboot support exists, you must first port
it to coreboot and, if it can then run without any blobs in the
image, it can be added to GNU Boot. See: [Motherboard Porting
Guide](http://www.coreboot.org/Motherboard_Porting_Guide) (this is just
the tip of the iceberg!)

Please note that board development should be done upstream (in coreboot)
and merged downstream (into GNU Boot). This is the correct way to do
it, and it is how the GNU Boot project is coordinated so as to avoid
too much forking of the coreboot source code.

What about ARM?
-----------------------------------

GNU Boot has support for some ARM based laptops, using the *Rockchip
RK3288* SoC. Check the GNU Boot [hardware compatibility
list](../docs/hardware/#supported_list), for more information.

General questions
=================

How do I install GNU Boot?
-------------------------------------------------------

See [installation guide](docs/install/)

How do I program an SPI flash chip?
---------------------------------------------------------------------------------

Refer to:\
[Externally rewrite 25xx NOR flash via SPI protocol](spi.md)

It's possible to use a 16-pin SOIC test clip on an 8-pin SOIC chip, if you
align the pins properly. The connection is generally more sturdy.

How do I write-protect the flash chip?
----------------------------------------------------------------------------

By default, there is no write-protection on a GNU Boot system. This is
for usability reasons, because most people do not have easy access to an
external programmer for re-flashing their firmware, or they find it
inconvenient to use an external programmer.

On some systems, it is possible to write-protect the firmware, such that
it is rendered read-only at the OS level (external flashing is still
possible, using dedicated hardware). For example, on current GM45
laptops (e.g. ThinkPad X200, T400), you can write-protect (see
[ICH9 gen utility](docs/install/ich9utils.md#ich9gen)).

It's possible to write-protect on all GNU Boot systems, but the instructions
need to be written. The documentation is in the main git repository, so you are
welcome to submit patches adding these instructions.

TODO: Document PRx based flash protection on Intel platforms, and investigate
other methods on AMD systems.

How do I change the BIOS settings?
------------------------------------------------------------------------

GNU Boot actually uses the [GRUB
payload](http://www.coreboot.org/GRUB2). More information about payloads
can be found at
[coreboot.org/Payloads](http://www.coreboot.org/Payloads). SeaBIOS is also
available. The *CMOS* config is hardcoded.

GNU Boot inherits the modular payload concept from coreboot, which
means that pre-OS bare-metal *BIOS setup* programs are not very
practical. Coreboot (and GNU Boot) include a utility called
*nvramtool*, which can be used to change some settings. You can find
nvramtool under *coreboot/util/nvramtool/*, in the GNU Boot source
archives.

The *-a* option in nvramtool will list the available options, and *-w*
can be used to change them. Consult the nvramtool documentation on the
coreboot wiki for more information.

In practise, you don't need to change any of those settings, in most
cases.

GNU Boot locks the CMOS table, to ensure consistent functionality for
all users. You can use:

    nvramtool -C yourimage.bin -w somesetting=somevalue

To get a full list of available options, do this:

    nvramtool -C yourimage.bin -a

This will change the default inside that image, and then you can
re-flash it.

How do I pad a image before flashing?
--------------------------------------

It is advisable to simply use a larger image. This section was written
mostly for ASUS KCMA-D8 and KGPE-D16 mainboards, where previously we only
provided 2MiB images in GNU Boot, but we now provide 16MiB images.
Other sizes are not provided because in practice, someone upgrading one of
these chips will just use a 16MiB one. Larger sizes are available, but 16MiB
is the maximum that you can use on all currently supported systems
that use SPI flash.

Required for images where the image is smaller than the flash chip
(e.g. writing a 2MiB image to a 16MiB flash chip).

Create an empty (00 bytes) file with a size the difference between
the image and flash chip. The case above, for example:

    truncate -s +14MiB pad.bin

For x86 descriptorless images you need to pad from the *beginning* of the image:

    cat pad.bin yourimage.bin > yourimage.bin.new

For ARM and x86 with intel flash descriptor, you need to pad after the image:

    cat yourimage.bin pad.bin > yourimage.bin.new

Flash the resulting file. Note that cbfstool will not be able to
operate on images padded this way so make sure to make all changes to
the image, including runtime config, before padding.

To remove padding, for example after reading it off the flash chip,
simply use dd(1) to extract only the non-padded portion. Continuing with the
examples above, in order to extract a 2MiB x86 descriptorless image from a
padded 16MiB image do the following:

    dd if=flashromread.bin of=yourimage.bin ibs=14MiB skip=1

With padding removed cbfstool will be able to operate on the image as usual.

Do I need to install a bootloader when installing a distribution?
---------------------------------------------------------------------------------------------------

GNU Boot integrates the GRUB bootloader already, as a
*[payload](http://www.coreboot.org/Payloads)*. This means that the GRUB
bootloader is actually *flashed*, as part of the boot firmware
(GNU Boot). This means that you do not have to install a boot loader on
the HDD or SSD, when installing a new distribution. You'll be able to
boot just fine, using the bootloader (GRUB) that is in the flash chip.

This also means that even if you remove the HDD or SSD, you'll still
have a functioning bootloader installed which could be used to boot a
live distribution installer from a USB flash drive. See
[How to install GNU+Linux on a GNU Boot system](../docs/gnulinux/grub_boot_installer.md)

Nowadays, other payloads are also provided. If you're using the SeaBIOS payload,
then the normal MBR bootsector is used on your HDD or SSD, like you would
expect. So the above paragraphs only apply to the GNU GRUB payload.

Do I need to re-flash when I re-install a distribution?
-------------------------------------------------------------------------------------------

Not anymore. Recent versions of GNU Boot (using the GRUB payload) will
automatically switch to a GRUB configuration on the HDD or SSD, if it
exists. You can also load a different GRUB configuration, from any kind
of device that is supported in GRUB (such as a USB flash drive). For
more information, see
[Modifying the GRUB Configuration in GNU Boot Systems](../docs/gnulinux/grub_cbfs.md)

If you're using the SeaBIOS payload, it's even easier. It works just like you
would expect. SeaBIOS implements a normal x86 BIOS interface.

What does a flash chip look like?
-----------------------------------------------------------------

You can find photos of various chip types on the following page:\
[External 25xx NOR flashing guide](docs/install/spi.md)

Who did the logo?
----------------------------------------------------------------

See the [license information](../../img/logo/license.md).

The Libreboot logo is available as a [bitmap](../../img/logo/logo.png), a
[vector](../../img/logo/logo.svg), or a [greyscale vector](../../img/logo/logo_grey.svg).

Libreboot Inside stickers are available as a
[PDF](../../img/logo/stickers/libreboot-inside-simple-bold-1.60cmx2.00cm-diecut-3.pdf) or
a
[vector](../../img/logo/stickers/libreboot-inside-simple-bold-1.60cmx2.00cm-diecut-3.svg)

You can find all of the available logos by browsing [this directory](img/logo/).

What other firmware exists outside of GNU Boot?
==================================================

The main freedom issue on any system, is the boot firmware (usually
referred to as a BIOS or UEFI). GNU Boot replaces the boot firmware
with fully free code, but even with GNU Boot, there may still be other
hardware components in the system (e.g. laptop) that run their own
dedicated firmware, sometimes proprietary. These are on secondary
processors, where the firmware is usually read-only, written for very
specific tasks. While these are unrelated to GNU Boot, technically
speaking, it makes sense to document some of the issues here.

Note that these issues are not unique to GNU Boot systems. They apply
universally, to most systems. The issues described below are the most
common (or otherwise critical).

Dealing with these problems will most likely be handled by a separate
project.

### External GPUs

The Video BIOS is present on most video cards. For integrated graphics,
the VBIOS (special kind of OptionROM) is usually embedded
in the main boot firmware. For external graphics, the VBIOS is
usually on the graphics card itself. This is usually proprietary; the
only difference is that SeaBIOS can execute it (alternatively, you embed it
in a coreboot image and have coreboot execute it, if you use a
different payload, such as GRUB).

On current GNU Boot systems, instead of VBIOS, coreboot native GPU init is used,
which is currently only implemented for Intel GPUs.
Other cards with proper KMS drivers can be initialized once Linux boots,
but copy of VBIOS may be still needed to fetch proper VRAM frequency
and other similar parameters (without executing VBIOS code).

In configurations where SeaBIOS and native GPU init are used together,
a special shim VBIOS is added that uses coreboot linear framebuffer.

NOTE: on desktop/server mainboards, coreboot is configured to load the option
ROM from an add-on GPU if present. This is the default behaviour on such systems
in GNU Boot.

### EC (embedded controller) firmware 

Most (all?) laptops have this. The EC (embedded controller) is a small,
separate processor that basically processes inputs/outputs that are
specific to laptops. For example:

-   When you flick the radio on/off switch, the EC will enable/disable
    the wireless devices (wifi, bluetooth, etc) and enable/disable an
    LED that indicates whether it's turned on or not
-   Listen to another chip that produces temperature readings, adjusting
    fan speeds accordingly (or turning the fan(s) on/off).
-   Takes certain inputs from the keyboard, e.g. brightness up/down,
    volume up/down.
-   Detect when the lid is closed or opened, and send a signal
    indicating this.
-   Etc.

EC is present on nearly all laptops. Other devices use, depending on complexity,
either EC or variant with firmware in Mask ROM - SuperIO.

### HDD/SSD firmware 

HDDs and SSDs have firmware in them, intended to handle the internal
workings of the device while exposing a simple, standard interface (such
as AHCI/SATA) that the OS software can use, generically. This firmware
is transparent to the user of the drive.

HDDs and SSDs are quite complex, and these days contain quite complex
hardware which is even capable of running an entire operating system (by
this, we mean that the drive itself is capable of running its own
embedded OS), even GNU+Linux or BusyBox/Linux.

SSDs and HDDs are a special case, since they are persistent storage
devices as well as computers.

Example attack that malicious firmware could do: substitute your SSH
keys, allowing unauthorized remote access by an unknown adversary. Or
maybe substitute your GPG keys. SATA drives can also have DMA (through
the controller), which means that they could read from system memory;
the drive can have its own hidden storage, theoretically, where it could
read your LUKS keys and store them unencrypted for future retrieval by
an adversary.

With proper IOMMU and use of USB instead of SATA, it might be possible
to mitigate any DMA-related issues that could arise.

Some proof of concepts have been demonstrated. For HDDs:
<https://spritesmods.com/?art=hddhack&page=1> For SSDs:
<http://www.bunniestudios.com/blog/?p=3554>

Viable free replacement firmware is currently unknown to exist. For
SSDs, the
[OpenSSD](http://www.openssd-project.org/wiki/The_OpenSSD_Project)
project may be interesting.

Apparently, SATA drives themselves don't have DMA but can make use of
it through the controller. This
<http://www.lttconn.com/res/lttconn/pdres/201005/20100521170123066.pdf>
(pages 388-414, 420-421, 427, 446-465, 492-522, 631-638) and this
<http://www.intel.co.uk/content/dam/www/public/us/en/documents/technical-specifications/serial-ata-ahci-spec-rev1_3.pdf>
(pages 59, 67, 94, 99).

The following is based on discussion with Peter Stuge (CareBear\\) in
the coreboot IRC channel on Friday, 18 September 2015, when
investigating whether the SATA drive itself can make use of DMA. The
following is based on the datasheets linked above:

According to those linked documents, FIS type 39h is *"DMA Activate FIS
- Device to Host"*. It mentions *"transfer of data from the host to
the device, and goes on to say: Upon receiving a DMA Activate, if the
host adapter's DMA controller has been programmed and armed, the host
adapter shall initiate the transmission of a Data FIS and shall transmit
in this FIS the data corresponding to the host memory regions indicated
by the DMA controller's context."* FIS is a protocol unit (Frame
Information Structure). Based on this, it seems that a drive can tell
the host controller that it would like for DMA to happen, but unless the
host software has already or will in the future set up this DMA transfer
then nothing happens. **A drive can also send DMA Setup**. If a DMA
Setup FIS is sent first, with the Auto-Activate bit set, then it is
already set up, and the drive can initiate DMA. The document goes on to
say *"Upon receiving a DMA Setup, the receiver of the FIS shall
validate the received DMA Setup request."* - in other words, the host
is supposed to validate; but maybe there's a bug there. The document
goes on to say *"The specific implementation of the buffer identifier
and buffer/address validation is not specified"* - so noone will
actually bother. *"the receiver of the FIS"* - in the case we're
considering, that's the host controller hardware in the chipset and/or
the kernel driver (most likely the kernel driver). All SATA devices have
flash-upgradeable firmware, which can usually be updated by running
software in your operating system; **malicious software running as root
could update this firmware, or the firmware could already be
malicious**. Your HDD or SSD is the perfect place for a malicious
adversary to install malware, because it's a persistent storage device
as well as a computer.

Based on this, it's safe to say that use of USB instead of SATA is
advisable if security is a concern. USB 2.0 has plenty of bandwidth for
many HDDs (a few high-end ones can use more bandwidth than USB 2.0 is
capable of), but for SSDs it might be problematic (unless you're using
USB 3.0, which is not yet usable in freedom. See

Use of USB is also not an absolute guarantee of safety, so do beware.
The attack surface becomes much smaller, but a malicious drive could
still attempt a "fuzzing" attack (e.g. sending malformed USB
descriptors, which is how the tyrant DRM on the Playstation 3 was
broken, so that users could run their own operating system and run
unsigned code). (you're probably safe, unless there's a security flaw
in the USB library/driver that your OS uses. USB is generally considered
one of the safest protocols, precisely because USB devices have no DMA)

Other links:

-   <https://www.vice.com/en_us/article/ypwkwk/the-nsas-undetectable-hard-drive-hack-was-first-demonstrated-a-year-ago>

It is recommended that you use full disk encryption, on HDDs connected
via USB. There are several adapters available online, that allow you to
connect SATA HDDs via USB. GNU Boot documents how to install several
distributions with full disk encryption. You can adapt these for use
with USB drives:

-   [Full disk encryption with Debian](../docs/gnulinux/encrypted_debian.md)
-   [Full disk encryption with Parabola](../docs/gnulinux/encrypted_parabola.md)

The current theory (unproven) is that this will at least prevent
malicious drives from wrongly manipulating data being read from or
written to the drive, since it can't access your LUKS key if it's only
ever in RAM, provided that the HDD doesn't have DMA (USB devices don't
have DMA). The worst that it could do in this case is destroy your data.
Of course, you should make sure never to put any keyfiles in the LUKS
header. **Take what this paragraph says with a pinch of salt. This is
still under discussion, and none of this is proven.**

### NIC (ethernet controller) 

Ethernet NICs will typically run firmware inside, which is responsible
for initializing the device internally. Theoretically, it could be
configured to drop packets, or even modify them.

With proper IOMMU, it might be possible to mitigate the DMA-related
issues. A USB NIC can also be used, which does not have DMA.

### CPU microcode 

Implements an instruction set. See 
description. Here we mean microcode built in to the CPU. We are not
talking about the updates supplied by the boot firmware (GNU Boot does
not include microcode updates, and only supports systems that will work
without it) Microcode can be very powerful. No proof that it's
malicious, but it could theoretically

There isn't really a way to solve this, unless you use a CPU which does
not have microcode. (ARM CPUs don't, but most ARM systems require blobs
for the graphics hardware at present, and typically have other things
like soldered wifi which might require blobs)

CPUs often on modern systems have a processor inside it for things like
power management. ARM for example, has lots of these.

### Sound card 

Sound hardware (integrated or discrete) typically has firmware on it
(DSP) for processing input/output. Again, a USB DAC is a good
workaround.

### Webcam 

Webcams have firmware integrated into them that process the image input
into the camera; adjusting focus, white balancing and so on. Can use USB
webcam hardware, to work around potential DMA issues; integrated webcams
(on laptops, for instance) are discouraged by the GNU Boot project.

### USB host controller 

Doesn't really apply to current GNU Boot systems (none of them have
USB 3.0 at the moment), but USB 3.0 host controllers typically rely on
firmware to implement the XHCI specification. Some newer coreboot ports
also require this blob, if you want to use USB 3.0.

This doesn't affect GNU Boot at the moment, because all current
systems that are supported only have older versions of USB available.
USB devices also don't have DMA (but the USB host controller itself
does).

With proper IOMMU, it might be possible to mitigate the DMA-related
issues (with the host controller).

### WWAN firmware 

Some laptops might have a simcard reader in them, with a card for
handling WWAN, connecting to a 3g/4g (e.g. GSM) network. This is the
same technology used in mobile phones, for remote network access (e.g.
internet).

NOTE: not to be confused with wifi. Wifi is a different technology, and
entirely unrelated.

The baseband processor inside the WWAN chip will have its own embedded
operating system, most likely proprietary. Use of this technology also
implies the same privacy issues as with mobile phones (remote tracking
by the GSM network, by triangulating the signal).

On some laptops, these cards use USB (internally), so won't have DMA,
but it's still a massive freedom and privacy issue. If you have an
internal WWAN chip/card, the GNU Boot project recommends that you
disable and (ideally, if possible) physically remove the hardware. If
you absolutely must use this technology, an external USB dongle is much
better because it can be easily removed when you don't need it, thereby
disabling any external entities from tracking your location.

Use of ethernet or Wi-Fi is recommended, as opposed to mobile networks,
as these are generally much safer.

On all current GNU Boot laptops, it is possible to remove the WWAN card
and sim card if it exists. The WWAN card is next to the Wi-Fi card, and
the sim card (if installed) will be in a slot underneath the battery, or
next to the RAM.

Operating Systems
=================

Can I use GNU+Linux?
--------------------------------------------------

Absolutely! It is well-tested in GNU Boot, and highly recommended. See
[installing GNU+Linux](../docs/gnulinux/grub_boot_installer.md) and
[booting GNU+Linux](../docs/gnulinux/grub_cbfs.md).

Any recent distribution should work, as long as it uses KMS (kernel mode
setting) for the graphics.

Fedora won't boot? (may also be applicable to Redhat/CentOS)
-----------------------------------------------------------

On Fedora, by default the grub.cfg tries to boot the Linux kernel in 16-bit
mode. You just have to modify Fedora's GRUB configuration.
Refer to [the GNU+Linux page](docs/gnulinux/).

Can I use BSD?
----------------------------------

Absolutely! GNU Boot has native support for NetBSD, OpenBSD and LibertyBSD.
Other distros are untested.

See:
[docs/bsd/](docs/bsd/)

Are other operating systems compatible?
-------------------------------------------------------------------

Unknown. Probably not.

Does GNU Boot make my machine 100% free?
==========================================

GNU Boot on all devices only provides host hardware init firmware images,
that can be written 25XX SPI NOR Flash. But on many systems there are
a lot more computers running blob firmware.
Some of them are not practical to replace due to being located on Mask ROM.
Some devices have EC firmware being build as well.
Additionally, besides software components, there are hardware ones
(from ICs to boards) that are not released on OSHW licenses.
We do not have a single device that would be "100% free",
and such absolutes are nearly impossible to reach.

Notable proprietary blobs (not a complete list):

* All devices
	* SATA/PATA Hard Drive/Optical Disc Drive Firmware
  ([often contain powerful ARM based computer](
  http://spritesmods.com/?art=hddhack&page=1))
	* Pendrives and any USB peripherals - they contain a computer
  with code running to at the very least handle the USB protocol
* ThinkPads:
	* EC Firmware (H8S until including Sandy Bridge, later ARC based MEC16xx)
	* TrackPoint Firmware (8051)
	* Penabled devices contain µPD78F0514 MCU on wacom subboard,
  and Atmega (AVR) on led indicator/button board
	* Battery BMS, bq8030 (CoolRISC C816)
* Chomebooks C201PA/C100PA:
	* Battery BMS, bq30z55
	* Elan Touchpad
	* eMMC [flash memory controller](https://en.wikipedia.org/wiki/Flash_memory_controller) firmware

Where can I learn more about electronics
==========================================

* Basics of soldering and rework by PACE  
    Both series of videos are mandatory regardless of your soldering skill.
    * [Basic Soldering](https://vid.puffyan.us/watch?v=vIT4ra6Mo0s&list=PL926EC0F1F93C1837)
    * [Rework and Repair](https://vid.puffyan.us/watch?v=HKX-GBe_lUI&list=PL958FF32927823D12)
* [edX course on basics of electronics](https://www.edx.org/course/circuits-and-electronics-1-basic-circuit-analysi-2)  
    In most countries contents of this course is covered during
    middle and high school. It will also serve well to refresh your memory
    if you haven't used that knowledge ever since.
* Impedance intro
    * [Similiarities of Wave Behavior](https://vid.puffyan.us/watch?v=DovunOxlY1k)
    * [Reflections in tranmission line](https://vid.puffyan.us/watch?v=y8GMH7vMAsQ)
    * Stubs:
        * [Wikipedia article on stubs](https://en.wikipedia.org/wiki/Stub_(electronics))
        * [Polar Instruments article on stubs](http://www.polarinstruments.com/support/si/AP8166.html)  
        With external SPI flashing we only care about unintended PCB stubs
* [How to accurately measure header/connector pitch](https://www.microcontrollertips.com/accurately-measure-headerconnector-pitch/)
* Other YouTube channels with useful content about electronics
    * [EEVblog](https://vid.puffyan.us/channel/UC2DjFE7Xf11URZqWBigcVOQ)
    * [Louis Rossmann](https://vid.puffyan.us/channel/UCl2mFZoRqjw_ELax4Yisf6w)
    * [mikeselectricstuff](https://vid.puffyan.us/channel/UCcs0ZkP_as4PpHDhFcmCHyA)
    * [bigclive](https://vid.puffyan.us/channel/UCtM5z2gkrGRuWd0JQMx76qA)
    * [ElectroBOOM](https://vid.puffyan.us/channel/UCJ0-OtVpF0wOKEqT2Z1HEtA)
    * [Jeri Ellsworth](https://vid.puffyan.us/user/jeriellsworth/playlists)
* Boardview files can be open with [OpenBoardview](https://github.com/OpenBoardView/OpenBoardView),
which is free software under MIT license.

Use of youtube-dl with mpv would be recommended for youtube links

Lastly the most important message to everybody gaining this wonderful new hobby - [Secret to Learning Electronics](https://vid.puffyan.us/watch?v=xhQ7d3BK3KQ)
