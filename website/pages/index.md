---
title: Free your BIOS today!
...

### What is this? ####

GNU Boot is a 100% free software project aimed at replacing the
nonfree boot software (like BIOS or UEFI) of computers with free boot
software.

GNU Boot is only a distribution: it reuses existing software projects
like Coreboot, GRUB, SeaBIOS, etc.

So it's not very different from [100% free GNU/Linux
distributions](https://www.gnu.org/distros/free-distros.html) like
Trisquel or Guix.

Like the other 100% free GNU/Linux distributions its tasks are:

* To find and remove nonfree software in projects it reuses.

* To build the software and assemble it in something that can be
  installed. To test it and to provide installation and upgrade
  instructions.

* To provide good default configuration that work for most users.

### How this project came to exist ####

We believe computer users deserve to control all the software they
run.  This belief is the key principle of the Free Software Movement,
and was the motive for developing the GNU operating system and
starting the Free Software Foundation.  We believe computer user
freedom is a crucial human rights.

In order to achieve our goal, it is crucial that we call a program
"free" or "libre" only when it is indeed free in its entirety.  When
we talk about specific programs, we must not muddle the facts about
what is free and what is not.  If people start referring to a progran
as "libre," when parts of it are not in fact libre, that tends to lead
the community astray.

Unfortunately, such a muddle happened last year with a boot program
that was free software and is called Libreboot: the development team
added nonfree code to it, but continued to use "Libre" in its name.

Libreboot was first released in 2013. It has been widely recommended
in the free software community for the last nine years. In November
2022, Libreboot began to include non-libre code.  We have made
repeated efforts to continue collaboration with those developers to
help their version of Libreboot remain libre, but that was not
successful.

Now we've stepped forward to stand up for freedom, ours and that of
the wider community, by maintaining our own version -- a genuinely libre
Libreboot, that after some hurdles gave birth to this project: GNU Boot.
